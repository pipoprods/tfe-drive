# 0.4.0

- Add FTP Support!
- Search in sdcards
- Clear cache after download queue is done
- New alert message if download queue is done with some errors
- Fix get Quota when refreshing token on google drive
- Fix image mime type (bug when opening images on non-owncloud webdavs)
- Fix Clickable files in view list mode
- Fix sound mime types
- Add ogg mime type for webdav/ftp
- Fix issue 5 with WebDAV and base URL having a tailing slash
- Fix issue 6 with WebDAV and HTML entities not being escaped

# 0.3.1

- Cancel previous search when opening a account
- Fix for fastmail webdav

# 0.3.0

- Add search for all apis minus webdav (todo?)
- Clear queue cancel current download too

# 0.2.2

- Fix box.com account login
- Fix open file on some file types

# 0.2.1

- Add depth parameter to webdav, to avoid error on some servers
- Fix error on webdav servers with no quota extension support


# 0.2.0

- Added a Upload / Download manager
- Added breadcrumb
- Change of google icons
- Added webdav account information
- Fix thumbnails size on owncloud/webdav (was forced to be squared before)
- Fix available file size for sdcards
- Choose between alternative link and open as text, if file not recognised

# 0.1.1

- Add account information (used vs available quota)
- Add txt/wav/mp3 mime types icons
- Add icons in preview container if no preview available
- Fix local file manager with phones with only 1 sdcard

# 0.1.0

- Local files thumbnails
- Browse local files directly from the app
- Share multiple files
- Better text reader 
- Fix download of big files
- Receive files from share function
- The cache images does not appear anymore on the gallery. You may need to clear the cache to remove old items.
- Clear cache option in settings
- Add donation link

# 0.0.7

- Explicit message when opening a file and the file is not supported by the phone.
- Fix copy/move to root directories
- Box.com api implementation
- Better thumbnail loading (3 by 3)
- Accounts list scrollbar if a lot of accounts created

# 0.0.5

- Change app icon
- File picker! compatible with google/dropbox/webdav 
- Onedrive (microsoft drive) integration
- Dropbox implementation
- Webdav/owncloud integration
- Fix download of google docs (using export links)

# 0.0.4

- Upload is now available for both internal or external cards.
- Enable/Disable cache images
- Choose on which sdcard put the cache files
- Display error if cache file creation fail
- Add warning about uploading individual files (thks mac from foxapps.eu)
- (ALPHA) Auto Sync folder - Upload or Download (every X minutes)

# 0.0.3

- Download folders and sub folders
- ru-RU translation
- better offline mode

# 0.0.2

- Fix settings github links

# 0.0.1

First commit
