
APIS: google / dropbox / webdav  / onedrive / box.com / internal?

- mkdir in root
- mkdir in subdirectory

- upload in root
- upload in subdirectory

- rename folder in root
- rename folder in subdirectory
- rename file in root
- rename file in subdirectory

- delete folder in root
- delete folder in subdirectory
- delete file in root
- delete file in subdirectory

- copy folder to root
- copy folder to subdirectory
- copy file to root
- copy file to subdirectory


