var Apiprot = function() {
    var self=this;
    this.accounts={};
};


Apiprot.prototype.initDb = function(retrying)
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        var request = indexedDB.open(self.dbname,2.3);
        request.onsuccess = function (e) {
            self.db = e.target.result;
            accounts.register(self.type,self.icon, self);
            ok();
        };
        request.onerror = function (e) {
            if(retrying)
            {
                console.error('FATAL ERROR! ',this);
                return reject();
            }
            console.error(e);
            console.err('error opening db: deleting', this);
            files.alert(translate('error_open_db', 10000));
            request = indexedDB.deleteDatabase(self.dbname);
            request.onsuccess= function()
            {
                self.initDb(1).then(ok, reject);
            };

            request.onerror= function()
            {
                console.error('FATAL ERROR! ',this);
                reject();
            };
            return;
            reject();
        };
        request.onupgradeneeded = function (e) {
            self.db = e.target.result;

            if (self.db.objectStoreNames.contains("labels")) {
                self.db.deleteObjectStore("labels");
            }
            if (self.db.objectStoreNames.contains("accounts")) {
                self.db.deleteObjectStore("accounts");
            }
            if (self.db.objectStoreNames.contains("feeds")) {
                self.db.deleteObjectStore("feeds");
            }
            if (self.db.objectStoreNames.contains("counts")) {
                self.db.deleteObjectStore("counts");
            }
            if (self.db.objectStoreNames.contains("items")) {
                self.db.deleteObjectStore("items");
            }
            var objectStore = self.db.createObjectStore('accounts', { keyPath: 'id', autoIncrement: true });
        };
    });

};

Apiprot.prototype.getAccounts = function()
{
    var self=this;
    var account = null;

    var transaction = self.db.transaction([ 'accounts' ]);
    var dbaccounts = transaction.objectStore('accounts');

    // open a cursor to retrieve all items from the 'notes' store
    dbaccounts.openCursor().onsuccess = function (e) {
        var cursor = e.target.result;
        if (cursor) {
            accounts.add(cursor.value, self);
            self.accounts[cursor.value.id] = cursor.value;
            cursor.continue();
        }
    };
};

Apiprot.prototype.deleteAccount = function(id)
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        if(!id)
        {
            reject();
        }
        delete self.accounts[id];
        var request = self.db.transaction(["accounts"], "readwrite")
            .objectStore("accounts")
            .delete(id);
        request.onsuccess = ok;
        request.onerror = reject;
    });
};


Apiprot.prototype.init = function()
{
    var self=this;
    this.accounts = {};


    if(this.callback)
    {
        this.callback(location.href);
    }
    this.initDb().then(function()
    {
        self.getAccounts();

        if(self.refresh_token)
        {
            self.refresh_token();
            self.refresh_interval=window.setInterval(function()
            {
                self.refresh_token();
            }, 1000*600 /* 10 minutes */);
        }
    });
};

Apiprot.prototype.setAccount = function(id)
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        var objectStore = self.db.transaction(["accounts"], "readwrite").objectStore("accounts");
        var request = objectStore.get(id);
        request.onerror = reject;
        request.onsuccess = function(event) {
            var data = request.result;
            self.account= data;
            if(!self.account.largestChangeId && self.getLargestChangeId)
            {
                self.getLargestChangeId();
            }
            ok();
            if(self.refresh_token)
            {
                self.refresh_token();
            }
        };
    });
};


Apiprot.prototype.open = function(received_item)
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        self.file_info(received_item.id).then(function(item)
        {
            // for bigger than 50Mo, do not download. open with online tool
            if(!item.download)
            {
                if(window.open(item.alternateLink))
                {
                    ok();
                }
                else
                {
                    reject();
                }
                return;
            }


            self._query("GET", item.download, null, true)
            .then(function(request)
            {
                var blob = request.response;
                blob.name=item.name;

                if(CustomActivity.isPicking)
                {
                    CustomActivity.send(blob);
                    return ok();
                }

                var activity = new Activity({
                    name: 'open',
                    data: {
                    type: blob.type,
                    allowSave: false,
                    blob: blob,
                    title: item.name
                    }
                });
                activity.onsuccess= ok;
                activity.onerror = function() {
                    console.error('error ',this.error);
                    if(this.error.name=='NO_PROVIDER' && item.alternateLink)
                    {
                        files.no_provider(blob, item.alternateLink);
                        ok();
                    }
                    else if(this.error.name==='NO_PROVIDER')
                    {
                        files.no_provider(blob);
                        ok();
                    }
                    else
                    {
                        console.error('big fail? ',this.error, item);
                        reject();
                    }
                };
            }, function(err)
            {
                reject();
            });
        },
        function()
        {
            reject();
        });
    });
};


Apiprot.prototype.download = function(ritem, destination)
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        self.file_info(ritem.id).then(function(item)
        {
            if(!item.download)
            {
                return reject(translate('download_not_available'));
            }
            self._query("GET", item.download, null, true)
            .then(function(request)
            {
                var blob = request.response;

                // Save to sdcard
                sdcard.add(blob, destination+item.name, true).then(function(result)
                {
                    ok();
                }, function(err)
                {
                    files.alert(translate('files_cannotcreate'), 7000, destination+item.name);
                    console.warn('ERROR write file!',destination+item.name,err);
                    reject(translate('download_error'));
                });
            }, function(err)
            {
                console.warn('error opening '+err, item);
                reject(translate('download_error'));
            });
        });
    });
};

Apiprot.prototype.share = function(item)
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        self.file_info(item.id).then(function(item)
        {
            if(!item.download)
            {
                return reject(translate('download_not_available'));
            }
            self._query("GET", item.download, null, true)
            .then(function(request)
            {
                var blob = request.response;
                ok(blob);
            }, function(err)
            {
                console.warn('error opening '+err);
                reject(translate('download_error'));
            });
        });
    });
};

Apiprot.prototype.list_dirs = function(id)
{
    var self=this;
    id=id.replace(self.account.id+'_','');

    var dirs={ parent: null, path:'', dirs: {}};
    return new Promise(function(ok, reject)
    {
        var dirs={ 'id':id, 'parent': null, path:'', dirs: {}};
        self._subdir_build(id, '', dirs).then(function(dirs)
        {
            ok(dirs);
        }, reject);
    });
};
Apiprot.prototype._subdir_build = function(id, current_path, current_dir)
{
    var self=this;
    id=id.replace(self.account.id+'_','');
    return new Promise(function(ok, reject)
    {
        self.list_dir(id).then(function(data)
        {
            var p = [];
            for(var i=0; i<data.items.length; i++)
            {
                var item = data.items[i];
                if(item.is_folder && !item.is_hidden && !item.is_trashed)
                {
                    if(!current_dir.dirs[item.name])
                    {
                        current_dir.dirs[item.name] ={ 'id': item.id,'path': current_path+item.name+'/','parent': current_dir, 'dirs': null };
                    }
                }
            }
            Promise.all(p).then(
                function()
                {
                    ok(current_dir);
                }, reject);
        }, function(err)
        {
            console.error('error fetch ',err);
            reject();
        });
    });
};

Apiprot.prototype.getMimeAndIcon = function(filename)
{
    if(/(jpe?g|png|exif|tiff|bmp|svg|gif)$/i.test(filename))
    {
        var ext = filename.replace(/^.*\./,'').toLowerCase();
        if(ext=='jpg') { ext='jpeg'; }
        mime = 'image/'+ext;
        icon='page_white_picture';
    }
    else if(/txt$/i.test(filename))
    {
        mime = 'text/plain';
        icon='page_white_text';
    }
    else if(/docx?$/i.test(filename))
    {
        mime = 'office/doc';
        icon='page_white_word';
    }
    else if(/xlsx?$/i.test(filename))
    {
        mime = 'office/xls';
        icon='page_white_excel';
    }
    else if(/pdf$/i.test(filename))
    {
        mime = 'application/pdf';
        icon='page_white_acrobat';
    }
    else if(/(mp3|wav|ogg)$/i.test(filename))
    {
        mime = 'audio/'+filename.replace(/^.*\./,'').toLowerCase();
        icon='music';
    }
    else if(/(avi|mp4|mov|mpeg)$/i.test(filename))
    {
        mime = 'video/'+filename.replace(/^.*\./,'').toLowerCase();
        icon='page_white_film';
    }
    else
    {
        mime = filename.indexOf('.')!==-1 ? filename.replace(/^.*\./,'').toLowerCase() : '???';
        icon='page_white';
    }
    return { mime: mime, icon: icon};
};



