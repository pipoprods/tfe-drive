var OneDrive = function(){};
OneDrive.prototype = new Apiprot();

OneDrive.prototype.dbname='OneDrive';
OneDrive.prototype.icon='onedrive';
OneDrive.prototype.type='OneDrive';
OneDrive.prototype.clientid = '000000004016C032';
OneDrive.prototype.clientsecret = 'wpz327VP5jibItq8gV47gCBKKT2K0azF';
OneDrive.prototype.auth_url = 'https://login.live.com/oauth20_authorize.srf';
OneDrive.prototype.token_url = 'https://login.live.com/oauth20_token.srf';
OneDrive.prototype.callback_url ='https://localhost/callback.html';
OneDrive.prototype.star_available = false;

OneDrive.prototype._query = function(method,url,data, custom_header , custom_account, skip_401)
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        // Init XHR object
        var r = new XMLHttpRequest({ mozSystem: true });
        r.open(method, url, true);
        if(!data || typeof data ==='string')
        {
            r.setRequestHeader("Content-type","application/x-www-form-urlencoded");
        }
        else
        {
            data = JSON.stringify(data);
            r.setRequestHeader("Content-type","application/json");
        }
        r.setRequestHeader("ts",new Date());
        if(custom_header)
        {
            r.responseType = "blob";
        }
        if(self.account || custom_account)
        {
            r.setRequestHeader("authorization","bearer "+(custom_account||self.account).access_token);
            r.setRequestHeader("Prefer","respond-async, wait=10000");
        }
        r.setRequestHeader("ts",new Date());

        r.onreadystatechange = function () {
            if (r.readyState == 4)
            {
                if(r.status == 401 && url!=self.token_url && self.account && !skip_401)
                {
                    console.error('401 ! refreshing token...', url, self.account.access_token);
                    self.refresh_token().then(function()
                            {
                                self._query(method,url,data, custom_header , custom_account, 1).then(ok, reject);
                            }, reject);
                }
                else if(r.status >= 200 && r.status< 400)
                {
                    return custom_header ? ok(r) :  ok(r.responseText);
                }
                else
                {
                    console.error('reject here ',r);
                    return reject(null);
                }
            }
        };
        r.send(data);
    });
};

OneDrive.prototype.callback = function(url)
{
    if(!/state=onedrive/.test(url))
    {
        return;
    }
    var self=this;
    files.alert(translate('getting_account_access'));
    var reCode = /code=([^&]+)/;
    var reError = /error=([^&]+)/;
    if((result = url.match(reError)))
    {
        files.alert(translate('error_get_token'));
    }
    else if((result = url.match(reCode)))
    {
        var code=result[1];
        var data  = 'code='+encodeURIComponent(code)+'&';
        data += 'client_id='+encodeURIComponent(this.clientid)+'&';
        data += 'client_secret='+encodeURIComponent(this.clientsecret)+'&';
        data += 'redirect_uri='+encodeURIComponent(this.callback_url)+'&';
        data += 'state=onedrive&';
        data += 'grant_type='+encodeURIComponent('authorization_code');

        this.getToken(data).
            then(function()
            {
            }, function()
            {
            });
    }
};

OneDrive.prototype.getToken = function(data)
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        files.alert(translate('getting_token'));
        self._query.bind(self)("POST", self.token_url, data)
        .then(function(text)
        {
            var data = JSON.parse(text);
            files.alert(translate('creating_account'));
            self.account=data;
            self.getProfile()
            .then(self.create_account_received.bind(self,data.access_token, data.refresh_token))
            .then(ok);
        }, reject);
    });
};

OneDrive.prototype.getProfile = function()
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        var r = new XMLHttpRequest({ mozSystem: true });
        r.open('GET', 'https://apis.live.net/v5.0/me?access_token='+self.account.access_token, true);
        r.setRequestHeader("ts",new Date());

        r.onreadystatechange = function () {
            if (r.readyState == 4)
            {
                if(r.status >= 200 && r.status<400)
                {
                    var data = JSON.parse(r.responseText);
                    self.userid = data.id;
                    self.email = data.name;
                    ok();
                }
                else
                {
                    console.error('error get user information',r);
                    reject();
                }
            }
        };
        r.send();
    });
};

OneDrive.prototype.create_account_received = function(access_token, refresh_token)
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        var transaction = self.db.transaction([ 'accounts' ], 'readwrite');
        //Create the Object to be saved i.e. our Note
        var value = {};
        value.access_token = access_token;
        value.refresh_token = refresh_token;
        value.userid = self.userid;
        value.email = self.email;
        value.id = self.userid;
        value.type = 'onedrive';

        var accounts_trans = transaction.objectStore('accounts');
        var request = accounts_trans.put(value);
        request.onsuccess = function (e) {
            accounts.add(value,self);
            ok();
        };
        request.onerror = function (e) {
            console.error('creating account error');
            reject();
        };
    });
};

OneDrive.prototype.create_account = function()
{
    var url = this.auth_url+'?'+
            'client_id='+encodeURIComponent(this.clientid)+'&'+
            'response_type=code&'+
            'redirect_uri='+encodeURIComponent(this.callback_url)+'&'+
            'scope=wl.skydrive_update wl.basic wl.offline_access&'+
            'state=onedrive&';
    window.open(url);
};

OneDrive.prototype.refresh_token = function()
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        var transaction = self.db.transaction([ 'accounts' ]);
        var dbaccounts = transaction.objectStore('accounts');

        var promises =[];
        // open a cursor to retrieve all items from the 'notes' store
        dbaccounts.openCursor().onsuccess = function (e) {
            var cursor = e.target.result;
            if (cursor) {
                var account = cursor.value;
                var data  = 'refresh_token='+encodeURIComponent(account.refresh_token)+'&';
                data += 'client_id='+encodeURIComponent(self.clientid)+'&';
                data += 'client_secret='+encodeURIComponent(self.clientsecret)+'&';
                data += 'grant_type='+encodeURIComponent('refresh_token');

                var promise = self._query.bind(self)("POST", self.token_url, data)
                .then(function(text)
                {
                    var data = JSON.parse(text);
                    if(self.account && account.id == self.account.id)
                    {
                        // Refresh 
                        self.account = account;
                    }
                    self.update_account(account,data.access_token.replace(':'+self.clientid,''))
                        .then(function()
                        {
                        }, function(err)
                        {
                            console.error('error refresh token ',err);
                        });
                }, function(err)
                {
                    console.error('error  update token',err,this);
                    reject();
                });
                promises.push(promise);
                cursor.continue();
            }
            else
            {
                Promise.all(promises).then(ok,  reject);
            }
        };
    });
};

OneDrive.prototype.update_account = function(obj,token)
{
    var account = null;
    var self=this;

    return new Promise(function(ok, reject)
    {
        var objectStore = self.db.transaction(["accounts"], "readwrite").objectStore("accounts");
        var request = objectStore.get(obj.id);
        request.onerror = reject;
        request.onsuccess = function(event) {
            var data = request.result;
            data.access_token = token;

            // Put this updated object back into the database.
            var requestUpdate = objectStore.put(data);
            requestUpdate.onerror = reject;
            requestUpdate.onsuccess = ok;
        };
    });
};

OneDrive.prototype.list_dir = function(id)
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        id=id.replace(self.account.id+'_','');

        var url;
        if(id=='/')
        {
            url =  'https://api.onedrive.com/v1.0/drive/root/children?expand=thumbnails';
        }
        else
        {
            url =  'https://api.onedrive.com/v1.0/drive/items/'+encodeURIComponent(id)+'/children?expand=thumbnails';
        }
        self._query.bind(self)("GET", url, null)
        .then(function(text)
        {
            var data = JSON.parse(text);
            var result={
                items: []
            };
            var files=[];
            var dirs=[];
            for(var i=0; i<data.value.length; i++)
            {
                var item = data.value[i];

                var created_item =   self.convertItem(id,item);
                if(created_item.folder)
                {
                    dirs.push(created_item);
                }
                else
                {
                    files.push(created_item);
                }
            }
            result.items = Array.concat(dirs, files);
            ok(result);
        },function(err)
        {
            console.error('error fetching! ',url,err);
            reject();
        });
    });
};

OneDrive.prototype.file_info = function(id)
{
    var self=this;
    id=id.replace(self.account.id+'_','');
    return new Promise(function(ok, reject)
    {
        var url =  'https://api.onedrive.com/v1.0/drive/items/'+encodeURIComponent(id)+'?expand=thumbnails&ts='+(new Date());
        self._query.bind(self)("GET", url, null)
        .then(function(text)
        {
            var item = JSON.parse(text);
            var parent= id;
            item = self.convertItem(parent,item);
            ok(item);
        }, reject);
    });
};

OneDrive.prototype.rename = function(id, newname)
{
    var self=this;
    id=id.replace(self.account.id+'_','');
    return new Promise(function(ok, reject)
    {
        self._query("PATCH",  'https://api.onedrive.com/v1.0/drive/items/'+id, { name: newname })
        .then(function(text)
        {
            ok();
        }, function(err)
        {
            reject();
        });
    });
};

OneDrive.prototype.recover = function(item)
{
    var self=this;

    var id =item.id;
    id=id.replace(self.account.id+'_','');
    return new Promise(function(ok, reject)
    {
        self._query("PATCH",  'https://www.OneDrive.com/drive/v2/files/'+id, { 'labels': { 'trashed': false } })
        .then(function(text)
        {
            ok();
        }, function(err)
        {
            reject();
        });
    });
};

OneDrive.prototype.realdelete = function(id)
{
    var self=this;
    id=id.replace(self.account.id+'_','');
    return new Promise(function(ok, reject)
    {
        self._query("DELETE",  'https://www.OneDrive.com/drive/v2/files/'+id)
        .then(function(text)
        {
            ok();
        }, function(err)
        {
            reject();
        });
    });
};

OneDrive.prototype.delete = function(id)
{
    var self=this;
    id=id.replace(self.account.id+'_','');
    return new Promise(function(ok, reject)
    {
        self._query("DELETE",  'https://api.onedrive.com/v1.0/drive/items/'+id)
        .then(function(text)
        {
            ok();
        }, function(err)
        {
            reject();
        });
    });
};

OneDrive.prototype.mkdir = function(id, newname)
{
    var self=this;
    id=id.replace(self.account.id+'_','');
    return new Promise(function(ok, reject)
    {
        if(id=='/') { id = 'root'; }

        self._query("POST",  'https://api.onedrive.com/v1.0/drive/items/'+encodeURIComponent(id)+'/children', { name: newname,  folder: {}})
        .then(function(text)
        {
            var received = JSON.parse(text);
            ok(received);
        }, function(err)
        {
            reject();
            console.warn('error mkdir '+err);
        });
    });
};

OneDrive.prototype.move = function(id, destination)
{
    var self=this;
    id=id.replace(self.account.id+'_','');
    if(id=='/') { id = 'root'; }

    destination=destination.replace(self.account.id+'_','');
    return new Promise(function(ok, reject)
    {
        self._query("PATCH",  'https://api.onedrive.com/v1.0/drive/items/'+id, { parentReference: { id: destination } })
        .then(function(text)
        {
            ok();
        }, function(err)
        {
            reject();
        });
    });
};

OneDrive.prototype.copy = function(id, destination)
{
    var self=this;
    id=id.replace(self.account.id+'_','');
    destination=destination.replace(self.account.id+'_','');
    if(destination=='/') { destination = 'root'; }

    return new Promise(function(ok, reject)
    {
        var param = destination!='root' ? {parentReference : { id: destination }}: {parentReference: {path:'/drive/root:' }};

        self._query("POST",  'https://api.onedrive.com/v1.0/drive/items/'+encodeURIComponent(id)+'/action.copy', param)
        .then(function(text)
        {
            ok();
        }, function(err)
        {
            reject();
        });
    });
};

OneDrive.prototype.create = function(id,blob)
{
    var self=this;
    id=id.replace(self.account.id+'_','');
    return new Promise(function(ok, reject)
    {
        id=id.replace(self.account.id+'_','');
        if(id=='/') { id = 'root'; }

        // Init XHR object
        var r = new XMLHttpRequest({ mozSystem: true });
        r.open('PUT', 'https://api.onedrive.com/v1.0/drive/items/'+encodeURIComponent(id)+'/children/'+blob.name.replace(/.*\//,'')+'/content');
        r.setRequestHeader("authorization","bearer "+self.account.access_token);

        r.onreadystatechange = function () {
            if (r.readyState == 4)
            {
                if(r.status >= 200 && r.status<400)
                {
                    return ok(r.responseText);
                }
                else
                {
                    console.error('fail create file ',r);
                    return reject(r.responseText);
                }
            }
        };
        r.send(blob);
    });
};

OneDrive.prototype.convertItem = function(id,item)
{
    var self=this;

    var filename = item.name;
    if(item.folder)
    {
        mime = translate('folder');
        icon='folder';
    }
    else
    {
        mimeData = this.getMimeAndIcon(filename);
        mime = mimeData.mime;
        icon=mimeData.icon;
    }

    var created=
    {
        parent: self.account.id+'_'+id,
        real_parent: id,
        id: self.account.id+'_'+item.id,
        realid: item.id,
        icon: 'img/icons/dropbox/'+(icon)+'.gif',
        name: item.name,
        mime: mime,
        creation_date: item.createdDateTime,
        modified_date: item.lastModifiedDateTime,
        size: item.size || null,
        thumbnail: item.thumbnails && item.thumbnails.length>0 ? item.thumbnails[0].medium.url : null,
        bigthumbnail: item.thumbnails && item.thumbnails.length>0 ? item.thumbnails[0].large.url : null,
        download: item['@content.downloadUrl'],
        webDownload: item.webContentLink,
        alternateLink: item.alternateLink,

        is_folder:  item.folder ? true: false,
        is_starred:  false,
        is_hidden:  false,
        is_trashed:  false,
    };
    return created;
};

OneDrive.prototype.getInfo = function(account)
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        var url =  'https://apis.live.net/v5.0/me/skydrive/quota?access_token='+account.access_token;
        var r = new XMLHttpRequest({ mozSystem: true });
        r.open('GET', url, true);
        r.setRequestHeader("ts",new Date());

        r.onreadystatechange = function () {
            if (r.readyState == 4)
            {
                if(r.status >= 200 && r.status<400)
                {
                    var data = JSON.parse(r.responseText);
                    var used = files.getReadableFileSizeString((parseInt(data.quota,10)-parseInt(data.available,10))/8);
                    var total = files.getReadableFileSizeString(parseInt(data.available,10)/8);
                    ok(used+' / '+total);
                }
                else
                {
                    reject();
                }
            }
        };
        r.send();
    });
};

OneDrive.prototype.search = function(search)
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        var url;
        url =  'https://api.onedrive.com/v1.0/drive/root/view.search?q='+encodeURIComponent(search)+'&expand=thumbnails';

        self._query.bind(self)("GET", url, null)
        .then(function(text)
        {
            var data = JSON.parse(text);
            var result={
                items: []
            };
            var files=[];
            var dirs=[];
            for(var i=0; i<data.value.length; i++)
            {
                var item = data.value[i];

                var created_item =   self.convertItem('search',item);
                if(created_item.folder)
                {
                    dirs.push(created_item);
                }
                else
                {
                    files.push(created_item);
                }
            }
            result.items = Array.concat(dirs, files);
            ok(result);
        },function(err)
        {
            console.error('error fetching! ',url,err);
            reject();
        });
    });
};

