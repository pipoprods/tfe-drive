var Google = function(){};
Google.prototype = new Apiprot();

Google.prototype.dbname='Google';
Google.prototype.icon='google';
Google.prototype.type='Google';
Google.prototype.clientid = '1031390938682-nc61btee95m1ueh25h0moufn9idavno0.apps.googleusercontent.com';
Google.prototype.clientsecret = 'wH6bKW_glOUVnPoBjJKaNgFR';
Google.prototype.auth_url = 'https://accounts.google.com/o/oauth2/auth';
Google.prototype.token_url = 'https://www.googleapis.com/oauth2/v3/token';
Google.prototype.callback_url ='https://localhost/callback.html';
Google.prototype.star_available=true;

Google.prototype._query = function(method,url,data, custom_header, custom_account, skip_401)
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        // Init XHR object
        var r = new XMLHttpRequest({ mozSystem: true });
        r.open(method, url, true);
        if(!data || typeof data ==='string')
        {
            r.setRequestHeader("Content-type","application/x-www-form-urlencoded");
        }
        else
        {
            data = JSON.stringify(data);
            r.setRequestHeader("Content-type","application/json");
        }
        r.setRequestHeader("ts",new Date());
        if(custom_header)
        {
            r.responseType = "blob";
        }
        if(self.account || custom_account)
        {
            r.setRequestHeader("authorization","OAuth "+(custom_account || self.account).access_token);
        }
        r.setRequestHeader("ts",new Date());

        r.onreadystatechange = function () {
            if (r.readyState == 4)
            {
                if(r.status == 401 && url!=self.token_url && (custom_account || self.account) && !skip_401)
                {
                    self.refresh_token(custom_account || self.account).then(function(account)
                            {
                                self._query(method,url,data, custom_header, account, 1).then(ok, reject);
                            }, reject);
                }
                else if(r.status >= 200 && r.status< 400)
                {
                    return custom_header ? ok(r) :  ok(r.responseText);
                }
                else
                {
                    console.error('reject here ',r);
                    return reject(null);
                }
            }
        };
        r.send(data);
    });
};

Google.prototype.callback = function(url)
{
    if(!/state=google/.test(url))
    {
        return;
    }
    var self=this;
    files.alert(translate('getting_account_access'));
    var reCode = /code=([^&]+)/;
    var reError = /error=([^&]+)/;
    if((result = url.match(reError)))
    {
        files.alert(translate('error_get_token'));
    }
    else if((result = url.match(reCode)))
    {
        var code=result[1];
        var data  = 'code='+encodeURIComponent(code)+'&';
        data += 'client_id='+encodeURIComponent(this.clientid)+'&';
        data += 'client_secret='+encodeURIComponent(this.clientsecret)+'&';
        data += 'redirect_uri='+encodeURIComponent(this.callback_url)+'&';
        data += 'state=google&';
        data += 'grant_type='+encodeURIComponent('authorization_code');

        this.getToken(data).
            then(function()
            {
            }, function()
            {
            });
    }
};

Google.prototype.getToken = function(data)
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        files.alert(translate('getting_token'));
        self._query.bind(self)("POST", self.token_url, data)
        .then(function(text)
        {
            var data = JSON.parse(text);
            files.alert(translate('creating_account'));
            self.account=data;
            self.getProfile()
            .then(self.create_account_received.bind(self,data.access_token.replace(':'+self.clientid,''), data.refresh_token))
            .then(ok);
        }, reject);
    });
};

Google.prototype.getProfile = function()
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        self._query.bind(self)("GET", 'https://www.googleapis.com/plus/v1/people/me', null)
        .then(function(text)
        {
            var data = JSON.parse(text);
            self.userid = data.etag;
            self.image = data.image ? data.image.url : null;
            self.email = data.emails && data.emails[0] ? data.emails[0].value : null;
            ok();
        }, function(err)
        {
            console.error('error get profile! ',err,this);
            reject();
        });
    });
};

Google.prototype.create_account_received = function(access_token, refresh_token)
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        var transaction = self.db.transaction([ 'accounts' ], 'readwrite');
        //Create the Object to be saved i.e. our Note
        var value = {};
        value.access_token = access_token;
        value.refresh_token = refresh_token;
        value.userid = self.userid;
        value.email = self.email;
        value.id = self.userid;
        value.type = 'google';

        var accounts_trans = transaction.objectStore('accounts');
        var request = accounts_trans.put(value);
        request.onsuccess = function (e) {
            accounts.add(value,self);
            ok();
        };
        request.onerror = function (e) {
            console.error('creating account error');
            reject();
        };
    });
};

Google.prototype.create_account = function()
{
    var url = this.auth_url+'?'+
            'response_type=code&'+
            'client_id='+encodeURIComponent(this.clientid)+'&'+
            'redirect_uri='+encodeURIComponent(this.callback_url)+'&'+
            'scope='+encodeURIComponent('https://www.googleapis.com/auth/drive')+' email&'+
            'state=google&'+
            'access_type=offline&'+
            'approval_prompt=force&'+
            'include_granted_scopes=true'
    ;
    window.open(url);
};

Google.prototype.refresh_token = function(account)
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        var transaction = self.db.transaction([ 'accounts' ]);
        var dbaccounts = transaction.objectStore('accounts');

        var promises =[];
        // open a cursor to retrieve all items from the 'notes' store
        dbaccounts.openCursor().onsuccess = function (e) {
            var cursor = e.target.result;
            var account;
            if (cursor) {
                var test_account = cursor.value;
                if(test_account)
                {
                    account = test_account;
                }
                var data  = 'refresh_token='+encodeURIComponent(account.refresh_token)+'&';
                data += 'client_id='+encodeURIComponent(self.clientid)+'&';
                data += 'client_secret='+encodeURIComponent(self.clientsecret)+'&';
                data += 'grant_type='+encodeURIComponent('refresh_token');

                var promise = self._query.bind(self)("POST", self.token_url, data)
                .then(function(text)
                {
                    var data = JSON.parse(text);
                    if(self.account && account.id == self.account.id)
                    {
                        // Refresh 
                        self.account = account;
                    }
                    self.update_account(account,data.access_token.replace(':'+self.clientid,''))
                        .then(function()
                        {
                        }, function(err)
                        {
                            console.error('error refresh token ',err);
                        });
                }, function(err)
                {
                    console.error('error  update token',err,this);
                    reject();
                });
                promises.push(promise);
                cursor.continue();
            }
            else
            {
                Promise.all(promises).then(function()
                {
                    ok(account);
                },  reject);
            }
        };
    });
};

Google.prototype.update_account = function(obj,token)
{
    var account = null;
    var self=this;

    return new Promise(function(ok, reject)
    {
        var objectStore = self.db.transaction(["accounts"], "readwrite").objectStore("accounts");
        var request = objectStore.get(obj.id);
        request.onerror = reject;
        request.onsuccess = function(event) {
            var data = request.result;
            data.access_token = token;

            // Put this updated object back into the database.
            var requestUpdate = objectStore.put(data);
            requestUpdate.onerror = reject;
            requestUpdate.onsuccess = ok;
        };
    });
};

Google.prototype.list_dir = function(id , pageToken)
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        id=id.replace(self.account.id+'_','');
        if(id==='/')
        {
            ok({
                items:[
                    {
                        parent: self.account.id+'_/',
                        id: self.account.id+'_root',
                        realid: 'root',
                        etag: 'root',
                        icon: 'img/icons/dropbox/folder.gif',
                        name: translate('root_folder'),
                        mime: '',
                        creation_date: null,
                        modified_date: null,
                        size: null,
                        thumbnail: null,
                        download: null,
                        special:true,
                        is_folder:  true,
                        is_starred:  true,
                        is_hidden:  false,
                        is_trashed:  false
                    },
                    {
                        parent: self.account.id+'_/',
                        id: self.account.id+'_special_shared',
                        realid: 'special_shared',
                        etag: 'shared',
                        icon: 'img/icons/dropbox/folder_user.gif',
                        name: translate('shared_with_me'),
                        mime: '',
                        creation_date: null,
                        modified_date: null,
                        size: null,
                        thumbnail: null,
                        download: null,
                        special:true,
                        is_folder:  true,
                        is_starred:  true,
                        is_hidden:  false,
                        is_trashed:  false
                    },
                    {
                        parent: self.account.id+'_/',
                        id: self.account.id+'_special_recent',
                        realid: 'special_recent',
                        etag: 'recent',
                        icon: 'img/recent.png',
                        name: translate('recent_files'),
                        mime: '',
                        creation_date: null,
                        modified_date: null,
                        size: null,
                        thumbnail: null,
                        download: null,
                        special:true,
                        is_folder:  true,
                        is_starred:  true,
                        is_hidden:  false,
                        is_trashed:  false
                    },
                    {
                        parent: self.account.id+'_/',
                        id: self.account.id+'_special_starred',
                        realid: 'special_starred',
                        etag: 'starred',
                        icon: 'img/icons/dropbox/folder_star.gif',
                        name: translate('starred'),
                        mime: '',
                        creation_date: null,
                        modified_date: null,
                        size: null,
                        thumbnail: null,
                        download: null,
                        special:true,
                        is_folder:  true,
                        is_starred:  true,
                        is_hidden:  false,
                        is_trashed:  false
                    },
                    {
                        parent: self.account.id+'_/',
                        id: self.account.id+'_special_trashed',
                        real: 'special_trashed',
                        etag: 'trashed',
                        icon: 'img/trashed.png',
                        name: translate('trashed'),
                        mime: '',
                        creation_date: null,
                        modified_date: null,
                        size: null,
                        thumbnail: null,
                        download: null,
                        special:true,
                        is_folder:  true,
                        is_starred:  true,
                        is_hidden:  false,
                        is_trashed:  false
                    }
                ]
            });
            return;
        }

        var query;
        if(id=='special_starred')
        {
                query = 'starred = true and trashed = false';
        }
        else if(id=='special_trashed')
        {
                query = 'trashed = true';
        }
        else if(id=='special_shared')
        {
                query = 'sharedWithMe = true and trashed = false';
        }
        else if(id=='special_recent')
        {
            var oneWeekAgo = new Date();
            oneWeekAgo.setDate(oneWeekAgo.getDate() - settings.getRecentTime());
            query = "  modifiedDate > '"+oneWeekAgo.toISOString()+"' and trashed = false ";
        }
        else
        {
            query="'"+id+"' in parents and trashed = false";
        }
        var url =  'https://www.googleapis.com/drive/v2/files?maxResults=1000&q='+encodeURIComponent(query);
        if(pageToken)
        {
            url+='&pageToken='+pageToken;
        }
        self._query.bind(self)("GET", url, null)
        .then(function(text)
        {
            var data = JSON.parse(text);
            var result={
                pageToken : data.nextPageToken,
                items: []
            };
            var files=[];
            var dirs=[];
            for(var i=0; i<data.items.length; i++)
            {
                var item = data.items[i];

                var created_item =   self.convertItem(id,item);
                if(created_item.is_folder)
                {
                    dirs.push(created_item);
                }
                else
                {
                    files.push(created_item);
                }
            }
            result.items = Array.concat(dirs, files);

            // Get next page, if not trash bin
            if(result.pageToken && id!=='special_trashed')
            {
                self.list_dir(id , result.pageToken).then(function(subresults)
                {
                    result.items = result.items.concat(subresults.items);
                    ok(result);
                }, reject);
            }
            else
            {
                ok(result);
            }
        },function(err)
        {
            console.error('error fetching! ',url,err);
            reject();
        });
    });
};
Google.prototype.search = function(search)
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        var query;
        query="title contains '"+search.replace("'","\\\'")+"' and trashed = false";

        var url =  'https://www.googleapis.com/drive/v2/files?maxResults=1000&q='+encodeURIComponent(query);

        self._query.bind(self)("GET", url, null)
        .then(function(text)
        {
            var data = JSON.parse(text);
            var result={
                pageToken : data.nextPageToken,
                items: []
            };
            var files=[];
            var dirs=[];
            for(var i=0; i<data.items.length; i++)
            {
                var item = data.items[i];

                var created_item =   self.convertItem('search',item);
                if(created_item.is_folder)
                {
                    dirs.push(created_item);
                }
                else
                {
                    files.push(created_item);
                }
            }
            result.items = Array.concat(dirs, files);

            ok(result);
        },function(err)
        {
            console.error('error fetching! ',url,err);
            reject();
        });
    });
};

Google.prototype.file_info = function(id)
{
    var self=this;
    id=id.replace(self.account.id+'_','');
    return new Promise(function(ok, reject)
    {
        var url =  'https://www.googleapis.com/drive/v2/files/'+id+'?ts='+(new Date());
        self._query.bind(self)("GET", url, null)
        .then(function(text)
        {
            var item = JSON.parse(text);
            var parent= item.parents.length>0 ? (item.parents[0].isRoot ? 'root': item.parents[0].id) : id;
            item = self.convertItem(parent,item);
            ok(item);
        }, reject);
    });
};

Google.prototype.rename = function(id, newname)
{
    var self=this;
    id=id.replace(self.account.id+'_','');
    return new Promise(function(ok, reject)
    {
        self._query("PATCH",  'https://www.googleapis.com/drive/v2/files/'+id, { title: newname })
        .then(function(text)
        {
            ok();
        }, function(err)
        {
            reject();
        });
    });
};

Google.prototype.star = function(id, status)
{
    var self=this;
    id=id.replace(self.account.id+'_','');
    return new Promise(function(ok, reject)
    {
        self._query("PATCH",  'https://www.googleapis.com/drive/v2/files/'+id, { 'labels': { 'starred': status } })
        .then(function(text)
        {
            ok();
        }, function(err)
        {
            reject();
        });
    });
};

Google.prototype.recover = function(item)
{
    var self=this;

    var id =item.id;
    id=id.replace(self.account.id+'_','');
    return new Promise(function(ok, reject)
    {
        self._query("PATCH",  'https://www.googleapis.com/drive/v2/files/'+id, { 'labels': { 'trashed': false } })
        .then(function(text)
        {
            ok();
        }, function(err)
        {
            reject();
        });
    });
};

Google.prototype.realdelete = function(id)
{
    var self=this;
    id=id.replace(self.account.id+'_','');
    return new Promise(function(ok, reject)
    {
        self._query("DELETE",  'https://www.googleapis.com/drive/v2/files/'+id)
        .then(function(text)
        {
            ok();
        }, function(err)
        {
            reject();
        });
    });
};

Google.prototype.delete = function(id)
{
    var self=this;
    id=id.replace(self.account.id+'_','');
    return new Promise(function(ok, reject)
    {
        self._query("POST",  'https://www.googleapis.com/drive/v2/files/'+id+'/trash', null)
        .then(function(text)
        {
            ok();
        }, function(err)
        {
            reject();
        });
    });
};

Google.prototype.mkdir = function(id, newname)
{
    var self=this;
    id=id.replace(self.account.id+'_','');
    return new Promise(function(ok, reject)
    {
        self._query("POST",  'https://www.googleapis.com/drive/v2/files/',
            {
                title: newname,
                parents: [{id:id}],
                mimeType: "application/vnd.google-apps.folder"
            }
            )
        .then(function(text)
        {
            var received = JSON.parse(text);
            ok(received);
        }, function(err)
        {
            reject();
            console.warn('error mkdir '+err);
        });
    });
};

Google.prototype.move = function(id, destination)
{
    var self=this;
    id=id.replace(self.account.id+'_','');
    destination=destination.replace(self.account.id+'_','');
    return new Promise(function(ok, reject)
    {
        self._query("PATCH",  'https://www.googleapis.com/drive/v2/files/'+id,
            {
                parents: [{ id:destination }]
            }
            )
        .then(function(text)
        {
            ok();
        }, function(err)
        {
            reject();
        });
    });
};

Google.prototype.copy = function(id, destination)
{
    var self=this;
    id=id.replace(self.account.id+'_','');
    destination=destination.replace(self.account.id+'_','');

    return new Promise(function(ok, reject)
    {
        self._query("POST",  'https://www.googleapis.com/drive/v2/files/'+id+'/copy',
            {
                parents: [{ id:destination }]
            }
            )
        .then(function(text)
        {
            ok();
        }, function(err)
        {
            reject();
        });
    });
};

Google.prototype.create = function(id,blob)
{
    var self=this;
    id=id.replace(self.account.id+'_','');
    return new Promise(function(ok, reject)
    {
        id=id.replace(self.account.id+'_','');
        // Init XHR object
        var r = new XMLHttpRequest({ mozSystem: true });
        r.open('POST', 'https://www.googleapis.com/upload/drive/v2/files?uploadType=multipart', true);
        data = JSON.stringify({
            title: blob.name.replace(/.*\//,''),
            parents: [{id:id}]
        });
        var boundary = (new Date()).getTime()+'_'+Math.ceil(Math.random()*10000);
        r.setRequestHeader("Content-Type","multipart/related; boundary=\""+boundary+"\"");
        r.setRequestHeader("authorization","OAuth "+self.account.access_token);

        r.onreadystatechange = function () {
            if (r.readyState == 4)
            {
                if(r.status >=200 && r.status<400)
                {
                    var received = JSON.parse(r.responseText);
                    if(received.id)
                    {
                        var r2 = new XMLHttpRequest({ mozSystem: true });
                        var url =  'https://www.googleapis.com/upload/drive/v2/files/'+received.id;
                        r2.open('PUT', url);
                        r2.setRequestHeader("authorization","OAuth "+self.account.access_token);
                        r2.onreadystatechange = function () {
                            if (r2.readyState == 4)
                            {
                                if(r2.status >=200 && r2.status<400)
                                {
                                    ok();
                                }
                                else
                                {
                                    console.error('set data ERROR!',r2);
                                    reject();
                                }
                            }
                        };
                        r2.send(blob);
                    }
                    else
                    {
                        console.error('error no file id :(');
                        reject();
                    }
                }
                else
                {
                    return reject(r.responseText);
                }
            }
        };
        r.send(
                "--"+boundary+"\r\n"+
                "Content-Type: application/json; charset=UTF-8\r\n"+
                "\r\n"+
                data+"\r\n"+
                "\r\n"+
                "--"+boundary+"\r\n"+
                "Content-Type: "+blob.type+"\r\n"+
                "Content-Transfer-Encoding: base64\r\n"+
                "\r\n"+

                ""+
                //base64data.replace(/^[^,]+,/,'')+"\r\n"+

                "--"+boundary+"--\r\n"+
                "\r\n"
        );

    });
};

Google.prototype.getLargestChangeId = function()
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        self._query("GET",  'https://www.googleapis.com/drive/v2/changes')
        .then(function(text)
        {
            var received = JSON.parse(text);

            var objectStore = self.db.transaction(["accounts"], "readwrite").objectStore("accounts");
            var request = objectStore.get(self.account.id);
            request.onerror = reject;
            request.onsuccess = function(event) {
                var data = request.result;
                data.largestChangeId=  received.largestChangeId;
                self.account.largestChangeId=data.largestChangeId;

                // Put this updated object back into the database.
                var requestUpdate = objectStore.put(data);
                requestUpdate.onerror = reject;
                requestUpdate.onsuccess = ok;
            };
        });
    });
};

Google.prototype.update = function()
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        if(!self.account.largestChangeId) { return reject(); }

        self._query("GET",  'https://www.googleapis.com/drive/v2/changes?startChangeId='+self.account.largestChangeId)
        .then(function(text)
        {
            var data = JSON.parse(text);
            if(data.largestChangeId=== self.account.largestChangeId)
            {
                ok();
            }
            else
            {
                // fetch changes ... here!
                var result=[];
                data.items.forEach(function(change)
                {
                    if(change.id!==self.account.largestChangeId)
                    {
                        if(change.deleted)
                        {
                            result.push({
                                id: change.fileId,
                                is_trashed: true
                            });
                        }
                        else if(change.file)
                        {
                            var item = change.file;
                            var parent= item.parents.length>0 ? (item.parents[0].isRoot ? 'root': item.parents[0].id) : id;
                            item = self.convertItem(parent, item);
                            result.push(item);
                        }
                    }
                });

                // save account settings
                var objectStore = self.db.transaction(["accounts"], "readwrite").objectStore("accounts");
                var request = objectStore.get(self.account.id);
                request.onerror = reject;
                request.onsuccess = function(event) {
                    var account = request.result;
                    account.largestChangeId=  data.largestChangeId;
                    self.account.largestChangeId=data.largestChangeId;

                    // Put this updated object back into the database.
                    var requestUpdate = objectStore.put(account);
                    requestUpdate.onerror = reject;
                    requestUpdate.onsuccess = ok(result);
                };
            }
        }, reject);
    });
};

Google.prototype.convertItem = function(id,item)
{
    var self=this;
    var mimeicon = this.getMimeAndIcon(item.name || '');
    var is_folder=false;
    if(item.mimeType=='application/vnd.google-apps.folder')
    {
        mimeicon.icon= 'folder';
        is_folder=true;
    }

    var created=
    {
        parent: self.account.id+'_'+id,
        real_parent: item.parents.length>0 ? (item.parents[0].isRoot ? 'root': item.parents[0].id) : id,
        id: self.account.id+'_'+item.id,
        realid: item.id,
        etag: item.etag,
        icon: mimeicon.icon ? 'img/icons/dropbox/'+mimeicon.icon+'.gif' : item.iconLink ,
        name: item.title,
        mime: item.mimeType,
        creation_date: item.createdDate,
        modified_date: item.modifiedDate,
        size: item.fileSize || null,
        thumbnail: item.thumbnailLink || null,
        bigthumbnail: item.thumbnailLink || null,
        download: item.downloadUrl || null,
        webDownload: item.webContentLink,
        alternateLink: item.alternateLink,

        is_folder:  is_folder,

        is_starred:  item.labels.starred,
        is_hidden:  item.labels.hidden,
        is_trashed:  item.labels.trashed,
    };
    if(!created.download)
    {
        if(item.exportLinks)
        {
            // Prefered mime types
            var prefered=new RegExp('vnd.openxmlformats');
            // @TODO: choose from which export link download.
            var keys= Object.keys(item.exportLinks);
            var selected_index =0;
            for(var i=0; i<keys.length;i++)
            {
                if(prefered.test(keys[i]))
                {
                    selected_index=i;
                }
            }
            if(item.exportLinks[keys[selected_index]].indexOf('exportFormat=')>-1)
            {
                created.name +=  "."+item.exportLinks[keys[selected_index]].replace(/.*=/,'');
            }
            created.download = item.exportLinks[keys[0]];
        }
    }
    return created;
};

Google.prototype.getInfo = function(account)
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        var url =  'https://www.googleapis.com/drive/v2/about';
        self._query("GET", url, null, null, account)
        .then(function(text)
        {
            var data = JSON.parse(text);
            var used = files.getReadableFileSizeString(parseInt(data.quotaBytesUsed,10));
            var total = files.getReadableFileSizeString(parseInt(data.quotaBytesTotal,10));
            ok(used+' / '+total);
        }, reject)  
    });
};
