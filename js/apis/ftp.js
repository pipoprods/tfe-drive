var Ftp = function(){};
Ftp.prototype = new Apiprot();

Ftp.prototype.dbname='Ftp';
Ftp.prototype.icon='ftp';
Ftp.prototype.type='Ftp';
Ftp.prototype.star_available = false;

Ftp.prototype.setAccount = function(id)
{
    var self=this;

    this.deco();

    return new Promise(function(ok, reject)
    {
        var objectStore = self.db.transaction(["accounts"], "readwrite").objectStore("accounts");
        var request = objectStore.get(id);
        request.onerror = reject;
        request.onsuccess = function(event) {
            var data = request.result;
            self.account= data;
            ok();
        };
    });
};

Ftp.prototype.deco = function()
{
    if(this.socket)
    {
        this.socket.close();
        this.socket=null;
    }
    if(this.pasv_socket)
    {
        this.pasv_socket.close();
        this.pasv_socket=null;
    }
};

Ftp.prototype.create_account_received = function(hostname, port, username, password)
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        var transaction = self.db.transaction([ 'accounts' ], 'readwrite');
        //Create the Object to be saved i.e. our Note
        var value = {};
        value.hostname = hostname;
        value.port = port;
        value.username = username;
        value.password = password;
        value.type = 'ftp';
        value.id = hostname+username;

        // To display account name
        value.email = username+'@'+hostname;

        var accounts_trans = transaction.objectStore('accounts');
        var request = accounts_trans.put(value);
        request.onsuccess = function (e) {
            accounts.add(value,self);
            ok();
        };
        request.onerror = function (e) {
            console.error('creating account error');
            reject();
        };
    });
};

Ftp.prototype.create_account = function()
{
    this.create_selector = new Selector();

    var description=document.createElement('div');

    var p = document.createElement('p');
    p.innerHTML=translate('ftp_help');
    description.appendChild(p);

    var form = document.createElement('form');
    description.appendChild(form);

    p = document.createElement('p');
    p.className='line_input';
    form.appendChild(p);


    ['ftp_hostname','ftp_port','ftp_username','ftp_password'].forEach(function(field)
    {
        var label = document.createElement('label');
        label.setAttribute('for',field);
        label.innerHTML=translate(field);
        p.appendChild(label);
        var input = document.createElement('input');
        if(field==='ftp_password') { input.setAttribute('type','password'); }
        else if(field==='ftp_port') { input.value='21'; }
        input.setAttribute('class',field);
        input.setAttribute('id',field);
        p.appendChild(input);
    });

    var items=[];
            items.push({
                'text':  translate('webdav_create'),
                'autoclose':false,
                'icon':  'plus',
                'callback':  this.create_form.bind(this)
            });

    this.create_selector.create(
            'selector_form',
            translate('ftp_creation'),
            description,
            items);
};

Ftp.prototype.login = function(hostname, port, username, password)
{
    var self=this;

    if(!hostname && this.account)
    {
        hostname = this.account.hostname;
        port = this.account.port;
        username = this.account.username;
        password = this.account.password;
    }

    return new Promise(function(ok, reject)
    {
        var socket = navigator.mozTCPSocket.open(hostname, port, { useSecureTransport: false,binaryType: 'string'});

        socket.onerror = function(e)
        {
            console.error('error socket ',e, this);
            self.socket=null;
            reject(translate('ftp_invalid_connect'));
        };
        socket.onclose = function(r)
        {
            console.log('error close socket ',r, this);
            self.socket=null;
        };
        socket.ondata = function(r)
        {
            var data = r.data;
            if(/^220/.test(data))
            {
                socket.send('USER '+username+'\r\n');
            }
            else if(/^331/.test(data))
            {
                socket.send('PASS '+password+'\r\n');
            }
            else if(/^230/.test(data))
            {
                self.socket=socket;
                ok();
            }
            else if(/^530/.test(data))
            {
                self.socket=null;
                socket.close();
                console.error('login fail2',data);
                reject(translate('ftp_invalid_login'));
            }
            else
            {
                self.socket=null;
                console.error('login fail3', data);
                socket.close();
                reject(translate('ftp_invalid_data', { command:data}));
            }
        };
    });
};

Ftp.prototype.pasv = function(cmd, resultstart, resultend, type, send_data, skip_reco)
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        self.command('EPSV', 229).then(function(data)
        {
            var re  = data.match(/\(\|{3}(\d+)\|\)/);
            if(re.length>0)
            {
                var port =parseInt(re[1],10);

                self.pasv_connect(self.account.hostname, port, type, send_data, ok).then(function()
                {
                    self.command(cmd, resultstart, resultend).then(function(x)
                    {
                        console.log('received final command ');
                        //if(self.pasv_socket) { self.pasv_socket.close(); }
                        //ok(self.pasv_data);
                    }, function()
                    {
                        if(self.socket) { self.socket.close(); }
                        console.error('error cmd !!');
                        reject();
                    });
                }, function()
                {
                    console.error('cannot pasv connect');
                    reject();
                });
            }
            else
            {
                reject();
            }
        }, function()
        {
            console.error('not in passsive!');
            reject();
        });
    });
};

Ftp.prototype.pasv_connect = function(hostname, port, type, send_data, ok_closed)
{
    var self=this;

    return new Promise(function(ok, reject)
    {
        console.log('connecting to ',hostname, port, type);
        if(self.pasv_socket)
        {
            self.pasv_socket.close();
        }

        self.pasv_socket = navigator.mozTCPSocket.open(hostname, port, { useSecureTransport: false, binaryType:type});
        self.pasv_data = type=='arraybuffer' ? [] : '';

        var send_buffer = null;
        var send_offset = 0;
        var send_row = 64*1024;

        self.pasv_socket.ondrain= function(e)
        {
            if(send_offset <= send_buffer.byteLength*8)
            {
                console.log('sending data...'+send_offset+' vs '+send_buffer.byteLength);
                self.pasv_socket.send(send_buffer, send_offset, send_row);
            }
            else
            {
                console.log('CLOSE SEND ',send_offset+' vs '+send_buffer.byteLength);
                self.pasv_socket.close();
            }
            send_offset += send_row;
        };
        self.pasv_socket.onopen = function(e)
        {
            console.log('connected pasv!',send_data);
            if(send_data)
            {
                fileReader = new FileReader();
                fileReader.onload = function() {
                    send_buffer = this.result;

                    if(self.pasv_socket.send(send_buffer.slice(send_offset, send_offset+send_row),send_offset, send_row))
                    {
                        self.pasv_socket.close();
                    }
                    else
                    {
                        send_offset += send_row;
                    }
                };
                fileReader.readAsArrayBuffer(send_data);
            }
            ok();
        };
        self.pasv_socket.onerror = function(e)
        {
            console.error('error socket ',e, this);
            reject(translate('ftp_invalid_connect'));
        };
        self.pasv_socket.onclose = function(r)
        {
            ok_closed(self.pasv_data);

            // STRANGE BUG HERE: forced to close the socket to be able to request more things...
            // Apparently we do not receive the whole data (ie 226 after complete data transfer,
            // etc...)
            // So we will reconnect to the ftp server for each pasv command done.
            if(self.socket) { self.socket.close(); }

            console.log('onclose!');
        };
        self.pasv_socket.ondata = function(r)
        {
            if(type=='arraybuffer')
            {
                self.pasv_data.push(r.data);
            }
            else
            {
                self.pasv_data+= r.data;
            }
        };
    });
};

Ftp.prototype.tofail = function(reject)
{
    this.cancelfail();
    this.fail_timer = window.setTimeout(function()
    {
        if(self.socket) { self.socket.close(); }
        self.fail_timer=null;
        reject();
    }, 500);
};

Ftp.prototype.cancelfail = function()
{
    window.clearTimeout(this.fail_timer);
};

Ftp.prototype.command = function(cmd, resultstart, resultend, skip_reco)
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        var restart = new RegExp('^('+resultstart+')');
        var reend = resultend ? new RegExp('^('+resultend+')') : null;
        if(self.socket)
        {
            self.socket.ondrain=function(r)
            {
                console.log('MAIN SOCKET DRAIN ',r);
            };
            self.socket.ondata=function(r)
            {
                var data = r.data;

                (data+'').split(/[\r\n]+/).map(function(line)
                {
                    if(line)
                    {
                        console.log('< '+line);
                        if(!resultend)
                        {
                            if(restart.test(line))
                            {
                                self.cancelfail();
                                ok(line);
                            }
                            else
                            {
                                console.error('error received unexpected '+line, resultstart,resultend, cmd);

                                // Wait a few seconds, if that was an error from a previous command?
                                self.tofail(reject);
                            }
                        }
                        else
                        {
                            if(reend.test(line))
                            {
                                console.log('reend ok !',reend);
                                ok(line);
                            }
                            else if(restart.test(line))
                            {
                            }
                            else if(!restart.test(line))
                            {
                                console.error('error received unexpected '+line, resultstart,resultend);
                                self.tofail(reject);
                            }
                        }
                    }
                });
            };
            console.log('> '+cmd);
            self.socket.send(cmd+'\r\n');
        }
        else if(!skip_reco)
        {
            self.login()
            .then(self.command.bind(self,cmd,resultstart, resultend, 1))
            .then(ok, reject);
        }
        else
        {
            reject();
        }
    });
};

Ftp.prototype.create_form = function()
{
    var self=this;

    var emptys =[];
    ['ftp_hostname','ftp_port'].forEach(function(field)
    {
        var dom_field = document.querySelector('#'+field);
        if(!dom_field.value)
        {
            emptys.push(translate(field));
        }
    });

    if(emptys.length>0)
    {
        return files.alert(translate('empty_fields', { fields: emptys.join(', ')}));
    }

    var hostname  = document.querySelector('#ftp_hostname').value;
    var port  = document.querySelector('#ftp_port').value;
    var username = document.querySelector('#ftp_username').value || 'anonymous';
    var password = document.querySelector('#ftp_password').value;

    files.alert(translate('ftp_checking'),6000);

    this.login(hostname,port,username,password).then(function()
    {
        self.create_account_received(hostname,port,username,password);
        files.alert(translate('creating_account'));
        self.create_selector.close();
    } , function(err)
    {
        files.alert(err);
    });
};

Ftp.prototype.list_dir = function(id)
{
    var self=this;

    id=id.replace(self.account.id+'_','');
    if(id=='root' || id=='')
    {
        id='/';
    }
    id= id.replace(/\/+/g,'/');

    return new Promise(function(ok, reject)
    {
        self.command('CWD '+id, 250)
        .then(function()
        {
            self.pasv('LIST', 150,226,'string',null)
            .then(function(buf)
            {
                var dirs = [];
                var files = [];
                (buf+'').split(/[\r\n]+/).map(function(line)
                {
                    var created_item =   self.convertItem(id,line);
                    if(created_item)
                    {
                        if(created_item.is_folder)
                        {
                            dirs.push(created_item);
                        }
                        else
                        {
                            files.push(created_item);
                        }
                    }
                });

                var result={
                    items:  {}
                };
                result.items = Array.concat(dirs, files);
                ok(result);
            }, reject);
        }, reject);
    });
};

Ftp.prototype.convertItem = function(id,line)
{
    var self=this;

    var re = new RegExp('^(.)'+ // directory or file
            '[^\\s]+\\s+'+ // rwx-rwx-rwx
            '[^\\s]+\\s+'+ // ??
            '[^\\s]+\\s+'+ // uid
            '[^\\s]+\\s+'+ // gid
            '([^\\s]+)\\s+'+ // size
            '[^\\s]+\\s+'+ // month
            '[^\\s]+\\s+'+ // day
            '[^\\s]+\\s+'+ // year or time
            '(.*)'); // filename

    if(result = line.match(re))
    {
        var is_dir = result[1]=='d';
        var size = result[2];
        var filename = result[3];
        if(filename=='.' || filename=='..')
        {
            return null;
        }

        var mime;
        var icon;
        if(is_dir)
        {
            mime = translate('folder');
            icon='folder';
        }
        else
        {
            mimeData = this.getMimeAndIcon(filename);
            mime = mimeData.mime;
            icon=mimeData.icon;
        }
        
        var created=
        {
            parent: self.account.id+'_'+id,
            real_parent: id,
            id: self.account.id+'_'+id+'/'+filename,
            realid: id+'/'+filename,
            icon: 'img/icons/dropbox/'+(icon)+'.gif',
            name: is_dir ? filename.replace(/^.*?([^\/]+)\/?$/,'$1') : filename.replace(/^.*\//,''),
            mime: mime,
            creation_date: 0,
            modified_date: null,
            size: parseInt(size,10),
            thumbnail: null,
            bigthumbnail: null,
            download: null,
            webDownload: null,
            alternateLink: null,

            is_folder:  is_dir,
            is_starred:  false,
            is_hidden:  false,
            is_trashed: false,
        };
        return created;
    }
};

Ftp.prototype.file_info = function(id)
{
    var self=this;
    id=id.replace(self.account.id+'_','');
    return new Promise(function(ok, reject)
    {
        var parent = id.replace(/[^\/]+\/?$/,'');
        id = '-rwxrwxrwx    0 0     0        0 0 0  0 '+id
        ok(self.convertItem(parent,id));
    });
};

Ftp.prototype.open = function(received_item)
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        var id = received_item.id;
        id=id.replace(self.account.id+'_','');

        var path = id.replace(/^(.*\/)[^\/]+$/,'$1').replace(/\/+/g,'/');
        var file = id.replace(/^(.*\/)([^\/]+)$/,'$2');

        self.command('CWD '+path, 250)
        .then(self.command.bind(self,'TYPE I', 200,null), reject)
        .then(self.pasv.bind(self,'RETR '+file, 150,226,'arraybuffer',null), reject)
        .then(function(data)
        {
            var mime_data = self.getMimeAndIcon(received_item.name);
            var blob = new Blob(data, { name: received_item.name, type: mime_data.mime });

            if(CustomActivity.isPicking)
            {
                CustomActivity.send(blob);
                return ok();
            }

            var activity = new Activity({
                name: 'open',
                data: {
                type: mime_data.mime,
                allowSave: false,
                blob: blob,
                title: received_item.name
                }
            });
            activity.onsuccess= ok;
            activity.onerror = function() {
                console.error('error ',this.error, mime_data,blob);
                if(this.error.name==='NO_PROVIDER')
                {
                    files.no_provider(blob);
                    ok();
                }
                else
                {
                    console.error('big fail? ',this.error, received_item);
                    reject();
                }
            };
        }, reject);
    });
};

Ftp.prototype.download = function(ritem, destination)
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        var id = ritem.id;
        id=id.replace(self.account.id+'_','');

        var path = id.replace(/^(.*\/)[^\/]+$/,'$1').replace(/\/+/g,'/');
        var file = id.replace(/^(.*\/)([^\/]+)$/,'$2');

        self.command('CWD '+path, 250)
        .then(self.command.bind(self,'TYPE I', 200,null), reject)
        .then(self.pasv.bind(self,'RETR '+file, 150,226,'arraybuffer',null), reject)
        .then(function(data)
        {
            var blob = new Blob(data);

            // Save to sdcard
            sdcard.add(blob, destination+ritem.name, true).then(function(result)
            {
                ok();
            }, function(err)
            {
                files.alert(translate('files_cannotcreate'), 7000, destination+ritem.name);
                reject(translate('download_error'));
            });
        }, reject);
    });
};

Ftp.prototype.rename = function(id, newname)
{
    var self=this;
    id=id.replace(self.account.id+'_','');
    return new Promise(function(ok, reject)
    {
        var path = id.replace(/[^\/]+$/, '').replace(/\/+/g,'/');
        var filename = id.replace(/^.*\//, '');

        self.command('CWD '+path, 250)
            .then(self.command.bind(self,'RNFR '+filename, 350,null,null))
            .then(self.command.bind(self,'RNTO '+newname, 250,null,null))
            .then(ok, reject);
    });
};

Ftp.prototype.delete = function(id, newname)
{
    var self=this;
    id=id.replace(self.account.id+'_','');
    return new Promise(function(ok, reject)
    {
        var path = id.replace(/[^\/]+$/, '').replace(/\/+/g,'/');
        var filename = id.replace(/^.*\//, '');

        self.command('CWD '+path, 250)
            .then(self.command.bind(self,'DELE '+filename, 250,null,null))
            .then(ok, reject);
    });
};

Ftp.prototype.mkdir = function(id, newname)
{
    var self=this;
    id=id.replace(self.account.id+'_','');
    return new Promise(function(ok, reject)
    {
        self.command('CWD '+id, 250)
            .then(self.command.bind(self,'MKD '+newname, 257,null,null))
            .then(ok, reject);
    });
};

Ftp.prototype.move = function(id, destination)
{
    var self=this;
    id=id.replace(self.account.id+'_','');
    destination=destination.replace(self.account.id+'_','');
    if(destination=='root')
    {
        destination='/';
    }
    return new Promise(function(ok, reject)
    {
        var path = id.replace(/[^\/]+$/, '').replace(/\/+/g,'/');
        var filename = id.replace(/^.*\//, '');

        self.command('CWD '+path, 250)
            .then(self.command.bind(self,'RNFR '+filename, 350,null,null))
            .then(self.command.bind(self,'RNTO '+destination+'/'+filename, 250,null,null))
            .then(ok, reject);
    });
};


Ftp.prototype.copy = function(id, destination)
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        id=id.replace(self.account.id+'_','');
        destination=destination.replace(self.account.id+'_','');
        if(destination=='root')
        {
            destination='/';
        }

        var path = id.replace(/^(.*\/)[^\/]+$/,'$1').replace(/\/+/g,'/');
        var file = id.replace(/^(.*\/)([^\/]+)$/,'$2');

        self.command('CWD '+path, 250)
        .then(self.command.bind(self,'TYPE I', 200,null), reject)
        .then(self.pasv.bind(self,'RETR '+file, 150,226,'arraybuffer',null), reject)
        .then(function(data)
        {
            console.log('data ',data,file);
            var blob = new Blob(data);
            blob.name = file;
            self.create(destination, blob).then(ok, reject)
        }, reject);
    });
};


Ftp.prototype.create = function(id,blob)
{
    var self=this;
    id=id.replace(self.account.id+'_','');
    return new Promise(function(ok, reject)
    {
        self.command('CWD '+id, 250)
        .then(self.command.bind(self,'TYPE I', 200,null), reject)
        .then(self.pasv.bind(self,'STOR '+blob.name.replace(/.*\//,''), 150,226,'arraybuffer', blob), reject)
        .then(function(data)
        {
            ok();
        }, reject);
    });
};


