var Dropbox = function(){};
Dropbox.prototype = new Apiprot();

Dropbox.prototype.dbname='Dropbox';
Dropbox.prototype.icon='dropbox';
Dropbox.prototype.type='Dropbox';
Dropbox.prototype.clientid = 'c15kopgbnbxtsjk';
Dropbox.prototype.auth_url =  'https://www.dropbox.com/1/oauth2/authorize';
Dropbox.prototype.token_url = 'https://www.dropbox.com/1/oauth2/token';
Dropbox.prototype.callback_url ='https://localhost/callback.html';
Dropbox.prototype.star_available = false;

Dropbox.prototype._query = function(method,url,data, custom_header, custom_account)
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        // Init XHR object
        var r = new XMLHttpRequest({ mozSystem: true });
        r.open(method, url, true);
        if(!data || typeof data ==='string')
        {
            r.setRequestHeader("Content-type","application/x-www-form-urlencoded");
        }
        else
        {
            data = JSON.stringify(data);
            r.setRequestHeader("Content-type","application/json");
        }
        r.setRequestHeader("ts",new Date());
        if(custom_header)
        {
            r.responseType = "blob";
        }
        if(self.account || custom_account)
        {
            r.setRequestHeader("authorization","Bearer "+(custom_account||self.account).access_token);
        }
        r.setRequestHeader("ts",new Date());

        r.onreadystatechange = function () {
            if (r.readyState == 4)
            {
                if(r.status >= 200 && r.status< 400)
                {
                    return custom_header ? ok(r) :  ok(r.responseText);
                }
                else
                {
                    console.error('fail query ',r);
                    return reject(null);
                }
            }
        };
        r.send(data);
    });
};

Dropbox.prototype.callback = function(url)
{
    if(!/state=dropbox/.test(url))
    {
        return;
    }
    var self=this;
    files.alert(translate('getting_account_access'));
    var reToken = /access_token=([^&]+)/;
    var reuid = /uid=([^&]+)/;
    var reError = /error=([^&]+)/;
    if((result = url.match(reError)))
    {
        files.alert(translate('error_get_token'));
    }
    else if((result = url.match(reToken)))
    {
        result_uid = url.match(reuid);
        var access_token=result[1];
        var uid=result_uid[1];

        self.account={ id: uid, access_token: access_token};
        self.getProfile()
        .then(self.create_account_received.bind(self,access_token));
    }
};

Dropbox.prototype.getProfile = function()
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        self._query.bind(self)("GET", 'https://api.dropboxapi.com/1/account/info', null)
        .then(function(text)
        {
            var data = JSON.parse(text);
            self.userid = data.uid;
            self.image = null;
            self.email = data.email;
            ok();
        }, function(err)
        {
            console.error('error get profile! ',err,this);
            reject();
        });
    });
};

Dropbox.prototype.create_account_received = function(access_token)
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        var transaction = self.db.transaction([ 'accounts' ], 'readwrite');
        //Create the Object to be saved i.e. our Note
        var value = {};
        value.access_token = access_token;
        value.userid = self.userid;
        value.email = self.email;
        value.id = self.userid;
        value.type = 'dropbox';

        var accounts_trans = transaction.objectStore('accounts');
        var request = accounts_trans.put(value);
        request.onsuccess = function (e) {
            accounts.add(value,self);
            ok();
        };
        request.onerror = function (e) {
            console.error('creating account error');
            reject();
        };
    });
};

Dropbox.prototype.create_account = function()
{
    var url = this.auth_url+'?'+
            'response_type=token&'+
            'client_id='+encodeURIComponent(this.clientid)+'&'+
            'redirect_uri='+encodeURIComponent(this.callback_url)+'&'+
            'force_reapprove=true&'+
            'state=dropbox'
    ;
    window.open(url);
};


Dropbox.prototype.update_account = function(obj,token)
{
    var account = null;
    var self=this;

    return new Promise(function(ok, reject)
    {
        var objectStore = self.db.transaction(["accounts"], "readwrite").objectStore("accounts");
        var request = objectStore.get(obj.id);
        request.onerror = reject;
        request.onsuccess = function(event) {
            var data = request.result;
            data.access_token = token;

            // Put this updated object back into the database.
            var requestUpdate = objectStore.put(data);
            requestUpdate.onerror = reject;
            requestUpdate.onsuccess = ok;
        };
    });
};

Dropbox.prototype.list_dir = function(id)
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        id=id.replace(self.account.id+'_','');
        if(id=='root')
        {
            id='/';
        }

        var query;
        var url;
        var method='GET';
        var data=null;
        var path_id = id;

        query="file_limit=10000&include_deleted=false&list=true&include_media_info=true";
        url =  'https://api.dropboxapi.com/1/metadata/auto/'+path_id+'?'+query;

        self._query.bind(self)(method, url, data)
        .then(function(text)
        {
            var data = JSON.parse(text);
            var result={
                items: []
            };
            var files=[];
            var dirs=[];
            for(var i=0; i<data.contents.length; i++)
            {
                var item = data.contents[i];

                var created_item =   self.convertItem(id,item);
                if(created_item.is_folder)
                {
                    dirs.push(created_item);
                }
                else
                {
                    files.push(created_item);
                }
            }
            result.items = Array.concat(dirs, files);
            ok(result);
        },function(err)
        {
            console.error('error fetching! ',url,err);
            reject();
        });
    });
};

Dropbox.prototype.file_info = function(id)
{
    var self=this;
    id=id.replace(self.account.id+'_','');
    return new Promise(function(ok, reject)
    {
        var query="file_limit=1000&include_deleted=false&list=true&include_media_info=true";
        var url =  'https://api.dropboxapi.com/1/metadata/auto/'+id+'?'+query;
        self._query.bind(self)("GET", url, null)
        .then(function(text)
        {
            var item = JSON.parse(text);
            var parent= id;
            item = self.convertItem(parent,item);
            ok(item);
        }, reject);
    });
};

Dropbox.prototype.rename = function(id, newname)
{
    var self=this;
    id=id.replace(self.account.id+'_','');
    return new Promise(function(ok, reject)
    {
        self._query("POST",  'https://api.dropboxapi.com/1/fileops/move', 'root=auto&from_path='+encodeURIComponent(id)+'&to_path='+encodeURIComponent(id.replace(/[^\/]+$/,newname)))
        .then(function(text)
        {
            ok();
        }, function(err)
        {
            reject();
        });
    });
};


Dropbox.prototype.delete = function(id)
{
    var self=this;
    id=id.replace(self.account.id+'_','');
    return new Promise(function(ok, reject)
    {
        self._query("POST",  'https://api.dropboxapi.com/1/fileops/delete', 'root=auto&path='+encodeURIComponent(id))
        .then(function(text)
        {
            ok();
        }, function(err)
        {
            reject();
        });
    });
};


Dropbox.prototype.mkdir = function(id, newname)
{
    var self=this;
    id=id.replace(self.account.id+'_','');
    //if(id=='root')   { id=''; }

    return new Promise(function(ok, reject)
    {
        self._query("POST",  'https://api.dropboxapi.com/1/fileops/create_folder', 'root=auto&path='+encodeURIComponent(id+'/'+newname))
        .then(function(text)
        {
            var received = JSON.parse(text);
            var createdid =  self.account.id+'_'+received.path;
            ok({ id : id+'/'+newname });
        }, function(err)
        {
            reject();
            console.warn('error mkdir '+err);
        });
    });
};

Dropbox.prototype.move = function(id, destination)
{
    var self=this;

    //if(destination=='root')   { destination=''; }
    id=id.replace(self.account.id+'_','');
    destination=destination.replace(self.account.id+'_','');
    if(destination=='root')
    {
        destination='/';
    }

    return new Promise(function(ok, reject)
    {
        self._query("POST",  'https://api.dropboxapi.com/1/fileops/move', 'root=auto&from_path='+encodeURIComponent(id)+'&to_path='+encodeURIComponent(destination)+'/'+id.replace(/^.*\//,''))
        .then(function(text)
        {
            ok();
        }, function(err)
        {
            reject();
        });
    });
};

Dropbox.prototype.copy = function(id, destination)
{
    var self=this;

    //if(destination=='root')   { destination=''; }
    id=id.replace(self.account.id+'_','');
    destination=destination.replace(self.account.id+'_','');
    if(destination=='root')
    {
        destination='/';
    }

    return new Promise(function(ok, reject)
    {
        self._query("POST",  'https://api.dropboxapi.com/1/fileops/copy', 'root=auto&from_path='+encodeURIComponent(id)+'&to_path='+encodeURIComponent(destination)+'/'+id.replace(/^.*\//,''))
        .then(function(text)
        {
            ok();
        }, function(err)
        {
            reject();
        });
    });
};

Dropbox.prototype.create = function(id,blob)
{
    var self=this;
    id=id.replace(self.account.id+'_','');
    return new Promise(function(ok, reject)
    {
        id=id.replace(self.account.id+'_','');
        // Init XHR object
        var r = new XMLHttpRequest({ mozSystem: true });
        r.open('POST', 'https://content.dropboxapi.com/1/files_put/auto/'+id+'/'+blob.name.replace(/^.*\//,''), '');
        var boundary = (new Date()).getTime()+'_'+Math.ceil(Math.random()*10000);
        r.setRequestHeader("Content-Type","multipart/related; boundary=\""+boundary+"\"");
        r.setRequestHeader("authorization","Bearer "+self.account.access_token);

        r.onreadystatechange = function () {
            if (r.readyState == 4)
            {
                if(r.status == 200)
                {
                    return ok(r.responseText);
                }
                else
                {
                    console.error('fail ? ',r.status);
                    return reject(r.responseText);
                }
            }
        };
        r.send(blob);
    });
};

Dropbox.prototype.convertItem = function(id,item)
{
    var self=this;
    var mime =item.path.replace(/^.*\./,'');
    if(/jpe?g|png|exif|tiff|bmp|svg|gif/i.test(mime))
    {
        mime='image/'+mime.toLowerCase();
    }
    
    var created=
    {
        parent: self.account.id+'_'+id,
        real_parent: id,
        id: self.account.id+'_'+item.path,
        realid: item.path,
        etag: item.etag,
        rev: item.rev,
        icon: 'img/icons/dropbox/'+item.icon+'.gif',
        name: item.path.replace(/^.*\//,'').replace(/^\//,''),
        mime: mime,
        creation_date: 0,
        modified_date: item.modified,
        size: item.bytes || null,
        thumbnail: item.thumb_exists ? 'https://content.dropboxapi.com/1/thumbnails/auto'+item.path+'?size=m' : null,
        bigthumbnail: item.thumb_exists ? 'https://content.dropboxapi.com/1/thumbnails/auto'+item.path+'?size=l' : null,
        download: 'https://content.dropboxapi.com/1/files/auto'+item.path,
        webDownload: null,
        alternateLink: null,

        is_folder:  item.is_dir,
        is_starred:  false,
        is_hidden:  false,
        is_trashed:  item.is_deleted,
    };
    if(!created.downloadUrl)
    {
        if(created.exportLinks)
        {
            // @TODO: choose from which export link download.
            var keys= Object.keys(created.exportLinks);
            created.downloadUrl = created.exportLinks[keys[0]];
        }
    }
    return created;
};

Dropbox.prototype.getInfo = function(account)
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        var url =  'https://api.dropboxapi.com/1/account/info';
        self._query("GET", url, null, null, account)
        .then(function(text)
        {
            var data = JSON.parse(text);
            var used = files.getReadableFileSizeString(parseInt(data.quota_info.normal,10));
            var total = files.getReadableFileSizeString(parseInt(data.quota_info.quota,10));
            ok(used+' / '+total);
        }, reject)  
    });
};

Dropbox.prototype.search = function(search)
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        var query;
        var url;
        var method='GET';
        var data=null;

        query="file_limit=10000&include_deleted=false&query="+encodeURIComponent(search);
        url =  'https://api.dropboxapi.com/1/search/auto/?'+query;

        self._query.bind(self)(method, url, query)
        .then(function(text)
        {
            var data = JSON.parse(text);
            var result={
                items: []
            };
            var files=[];
            var dirs=[];
            for(var i=0; i<data.length; i++)
            {
                var item = data[i];

                var created_item =   self.convertItem('search',item);
                if(created_item.is_folder)
                {
                    dirs.push(created_item);
                }
                else
                {
                    files.push(created_item);
                }
            }
            result.items = Array.concat(dirs, files);
            ok(result);
        },function(err)
        {
            console.error('error fetching! ',url,err);
            reject();
        });
    });
};

