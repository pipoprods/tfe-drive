String.prototype.unescapeHTML = function () {
    var temp = document.createElement("div");
    temp.innerHTML = this;
    var result = temp.textContent;
    temp.removeChild(temp.firstChild);
    return result;
}

String.prototype.escapeHTML = function () {
    var temp = document.createElement("div");
    temp.textContent = this;
    var result = temp.innerHTML;
    temp.removeChild(temp.firstChild);
    return result;
}
