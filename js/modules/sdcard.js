var Sdcard = function()
{
    var self=this;
    this.type='sdcard';
    this.cachedir='cache/';
};


Sdcard.prototype.init = function()
{
    var self=this;
    this.prefix=translate('app_title')+'/';
    this.sdcards = navigator.getDeviceStorages('sdcard');
    this.sdcard = this.sdcards[0];

    this.cache = {};
    this.sdcards.forEach(function(sdcard_sub)
    {
        sdcard_sub.onchange= self.update.bind(self);
    });
    this.list_dirs();
};

Sdcard.prototype.list_sdcard = function(sdcard_sub)
{
    var self=this;

    return new Promise(function(ok, reject)
    {
        if(self.cache[sdcard_sub.storageName||'default'])
        {
            return ok(self.cache[sdcard_sub.storageName||'default']);
        }
        // Let's browse all the images available
        var cursor = sdcard_sub.enumerate();

        var files=[];
        cursor.onsuccess = function() 
        {
            var file = this.result;
            if(file)
            {
                files.push(file);
            } 
            // Once we found a file we check if there is other results
            if (!this.done)
            {
                this.continue();
            }
            else
            {
                self.cache[sdcard_sub.storageName||'default'] = files;
                ok(files);
            }
        };
    });
};

Sdcard.prototype.update = function(e)
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        var promises=[];
        self.cache={};
        self.sdcards.forEach(function(sdcard_sub)
        {
            promises.push(self.list_sdcard(sdcard_sub));
        });
        Promise.all(promises).then(ok, reject);
    });
};

Sdcard.prototype.getfile = function(path)
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        var sdcard_selected=null;
        self.sdcards.forEach(function(sdcard_sub)
        {
            var re = new RegExp('^\\/?'+sdcard_sub.storageName);
            if(re.test(path))
            {
                sdcard_selected=sdcard_sub;
                path= path.replace(re,'').replace(/^\/+/,'');
            }
        });
        if(!sdcard_selected)
        {
            console.error('cannot choose which sdcard to use ',path, self.sdcards);
            return reject();
        }

        // Check if available on device
        request = sdcard_selected.get(path);
        request.onsuccess = function() { 
            var file = this.result;
            ok(file);
        };
        request.onerror = reject;
    });
};
Sdcard.prototype.update_settings = function()
{
    var self=this;
    self.cache_enabled = settings.getEnableCache();

    var sdcard_selected=null;
    var storagename = settings.getCacheLocation();

    self.sdcards.forEach(function(sdcard_sub)
    {
        if(sdcard_sub.storageName==storagename)
        {
            sdcard_selected=sdcard_sub;
        }
    });
    if(!sdcard_selected)
    {
        sdcard_selected = self.sdcards[0];
    }
    self.cache_sdcard = sdcard_selected;
};


Sdcard.prototype.clear_cache_image = function()
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        var fetch_sdcard=  self.cache_sdcard;
        var dir = fetch_sdcard.storageName+'/'+self.prefix+self.cachedir;
        var promises=[];
        self.list_files(dir).then(function(files)
        {
            files.forEach(function(file)
            {
                promises.push(self.delete(file, true));
            });
        });
        Promise.all(promises).then(ok, reject);
    });
};

Sdcard.prototype.cache_image = function(id,url)
{
    var self=this;
    return new Promise(function(ok, reject)
    {

        id = id.replace(/[\/"']/g,'');
        var fetch = self.prefix+self.cachedir+id+'.cache';
        var fetch_sdcard=  self.cache_sdcard;

        // If cache not needed (relative app file)
        if(!/^(http|sdcard)/.test(url))
        {
            ok(url);
        }
        else
        {
            // Check if available on device
            request = fetch_sdcard.get(fetch);
            request.onsuccess = function() {
                var file = this.result;
                var mysrc = URL.createObjectURL(file);
                if(file.size)
                {
                    ok(mysrc);
                }
                else
                {
                    console.error('empty cache image file, fetching new one',file);
                    var request = fetch_sdcard.delete(fetch);
                    request.onsuccess = function()
                    {
                        self.cache_image_net(id,url).then(ok, reject);
                    };
                    request.onerror = function()
                    {
                        console.error('cannot delete empty cache image file');
                        reject();
                    };
                }
            };
            request.onerror = function(e)
            {
                // on sdcard files, wait a little, to avoid cpu throttle
                if(/^sdcard/.test(url))
                {
                    window.setTimeout(function()
                    {
                        self.cache_image_net(id,url).then(ok,reject);
                    }, 50);
                }
                else
                {
                    self.cache_image_net(id,url).then(ok,reject);
                }
            };
        }
    });
};

Sdcard.prototype.cache_image_net = function(id,url)
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        //  Generate thumbnail from the sdcard
        if(/^sdcard:\/\//.test(url))
        {
            var regex = /^sdcard:\/\/(.*?)(?:@(\d+)x(\d+))?$/;
            var re;
            if(re = url.match(regex))
            {
                var file = re[1];
                var width = re[2]; 
                var height = re[3]; 
                self.getfile(file).then(function(file)
                {
                    if(!width || !height)
                    {
                        ok(window.URL.createObjectURL(file));
                        return;
                    }
                    var canvas = document.createElement('canvas');

                    var img = new Image;
                    img.onload = function()
                    {
                        canvas.width=img.width;
                        canvas.height=img.height;
                        var ctx = canvas.getContext('2d');
                        ctx.drawImage(img, 0, 0);

                        resample_hermite(canvas, img.width, img.height, width);

                        canvas.toBlob(function(blob)
                        {
                            var request = self.cache_sdcard.addNamed(blob, self.prefix+self.cachedir+id+'.cache');

                            // If file saved success
                            request.onsuccess = function() {
                                ok(URL.createObjectURL(blob));
                            };
                            request.onerror = function() {
                                // File already present...
                                if(this.error.name=='NoModificationAllowedError')
                                {
                                    ok(URL.createObjectURL(blob));
                                }
                                else
                                {
                                    console.error('cannot write sdcard cache file with content! ',this, self.cache_sdcard,  self.prefix+self.cachedir+id+'.cache');
                                    ok(URL.createObjectURL(blob));
                                }
                            };
                        });
                    };
                    img.onerror = reject;
                    img.src = URL.createObjectURL(file);
                }, function()
                {
                    console.error('error sdcard file ',file);
                    reject();
                });
            }
            else
            {
                reject();
            }
        }
        else
        {
            // if not available, download and save!
            var r = new XMLHttpRequest({ mozSystem: true });
            r.open('GET', url, true);
            r.responseType = "blob";
            if(files.api.account && (files.api.type=='Dropbox' || files.api.type=='OneDrive' || files.api.type=='Box'))
            {
                r.setRequestHeader("authorization","Bearer "+files.api.account.access_token);
            }
            else if(files.api.account && (files.api.type=='Google'))
            {
                r.setRequestHeader("authorization","OAuth "+files.api.account.access_token);
            }
            else if(files.api.account && files.api.type=='Webdav')
            {
                r.setRequestHeader("Authorization","Basic "+btoa(files.api.account.username+":"+files.api.account.password));
            }
            r.onreadystatechange = function () {
                if (r.readyState == 4)
                {
                    // not available yet, wait a little
                    if(r.status ==202)
                    {
                        window.setTimeout(function()
                        {
                            self.cache_image_net(id,url).then(ok,reject);
                        }, 2000);
                    }
                    else if(r.status >= 200 && r.status<400)
                    {
                        var blob = r.response;
                        if(!settings.getEnableCache())
                        {
                            var reader = new window.FileReader();
                            reader.readAsDataURL(blob); 
                            reader.onloadend = function() {
                                // return base64 encoded value
                                ok(reader.result);
                            };
                        }
                        else
                        {
                            var request = self.cache_sdcard.addNamed(blob, self.prefix+self.cachedir+id+'.cache');

                            // If file saved success
                            request.onsuccess = function() {
                                var reader = new window.FileReader();
                                reader.readAsDataURL(blob); 
                                reader.onloadend = function() {
                                    // return base64 encoded value
                                    ok(reader.result);
                                };
                            };
                            request.onerror = function() {
                                // File already present...
                                if(this.error.name=='NoModificationAllowedError')
                                {
                                    var reader = new window.FileReader();
                                    reader.readAsDataURL(blob); 
                                    reader.onloadend = function() {
                                        // return base64 encoded value
                                        ok(reader.result);
                                    };
                                }
                                else
                                {
                                    console.error('cannot write cache file with content! ',this, self.cache_sdcard,  self.prefix+self.cachedir+id+'.cache');
                                    reject();
                                }
                            };
                        }
                    }
                    else
                    {
                        reject();
                    }
                }
            };
            r.send();
        }
    });
};

Sdcard.prototype.add = function(blob, file , absolute_path)
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        var sdcard_selected = self.sdcard;
        // Remove storageName from sdcard
        if(absolute_path)
        {
            self.sdcards.forEach(function(sdcard_sub)
            {
                var re = new RegExp('^\\/?'+sdcard_sub.storageName);
                if(re.test(file))
                {
                    sdcard_selected=sdcard_sub;
                }
            });
            if(!sdcard_selected)
            {
                console.error('cannot choose which sdcard to use ',path, self.sdcards);
                return reject();
            }

            var re = new RegExp('^\/?'+sdcard_selected.storageName+'\/');
            file=file.replace(re,'');
        }
        else
        {
            file = self.prefix+file;
        }

        file = file.replace(/\/+/g,'/').replace(/^\//,'');

        // Check if existing file exists
        request = sdcard_selected.delete(file);

        // file existed
        request.onsuccess = function() { 
            var request = sdcard_selected.addNamed(blob, file);
            request.onsuccess = ok;
            request.onerror = function()
            {
                console.error('error addnamed ',this);
                reject();
            };
        };

        // file was not existing, but creating a new one anyway
        request.onerror = function() { 
            var request = sdcard_selected.addNamed(blob,file);
            request.onsuccess = ok;
            request.onerror = function()
            {
                console.error('error addNamed2 ',this);
                reject();
            };
        };
        
    });
};

Sdcard.prototype.delete = function(file , absolute_path)
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        var sdcard_selected = self.sdcard;
        // Remove storageName from sdcard
        if(absolute_path)
        {
            self.sdcards.forEach(function(sdcard_sub)
            {
                var re = new RegExp('^\\/?'+sdcard_sub.storageName);
                if(re.test(file))
                {
                    sdcard_selected=sdcard_sub;
                }
            });
            if(!sdcard_selected)
            {
                console.error('cannot choose which sdcard to use ',path, self.sdcards);
                return reject();
            }

            var re = new RegExp('^\/?'+sdcard_selected.storageName+'\/');
            file=file.replace(re,'');
        }
        else
        {
            file = self.prefix+file;
        }
        // Check if existing file exists
        var request = sdcard_selected.delete(file);

        // file existed
        request.onsuccess = ok;
        request.onerror = reject;
    });
};

Sdcard.prototype.list_dirs = function(skip_cache)
{
    var self=this;
    var dirs={ parent: null, path:'', dirs: {}};
    return new Promise(function(ok, reject)
    {
        var ended=0;

        var promises = [];
        self.sdcards.forEach(function(sdcard_sub)
        {
            promises.push(self.list_sdcard(sdcard_sub).then(function(files)
            {
                files.forEach(function(file)
                {
                    var name = file.name;
                    var dir = name.replace(/[^\/]+$/,'');
                    var segments = dir.split(/\//);
                    var current_dir = dirs;
                    var current_path = '';
                    for(var i=0; i<segments.length; i++)
                    {
                        var segment = segments[i];
                        if(segment)
                        {
                            if(!current_dir.dirs[segment])
                            {
                                current_dir.dirs[segment] ={ 'path': current_path+segment+'/','parent': current_dir, 'dirs': {} };
                            }
                            current_dir = current_dir.dirs[segment];
                            current_path+=segment+'/';
                        }
                    }
                }); 
            }, function()
            {
                console.error('error listing ',sdcard_sub);
            }));
        });
        Promise.all(promises).then(function()
        {
            ok(dirs);
        }, reject);
    });
};

Sdcard.prototype.list_files = function(path, fullinfo)
{
    var self=this;
    var files=[];


    path = path.replace(/^\/+/,'');
     var re = new RegExp('^\\/?'+path.replace(/\//g,'\\/')+'[^\\/]+$');
     var notEmpty = new RegExp('\/\.empty');
    return new Promise(function(ok, reject)
    {
        var sdcard_selected=null;
        self.sdcards.forEach(function(sdcard_sub)
        {
            var re = new RegExp('^\\/?'+sdcard_sub.storageName);
            if(re.test(path))
            {
                sdcard_selected=sdcard_sub;
            }
        });
        if(!sdcard_selected)
        {
            if(!path && self.sdcards.filter(function(x) { return x.storageName!==''}).length>0)
            {
                return ok([]);
            }
            else
            {
                console.error('cannot choose which sdcard to use ',path, self.sdcards);
                return reject();
            }
        }

        self.list_sdcard(sdcard_selected).then(function(received_files)
        {
            received_files.forEach(function(file)
            {
                if(re.test(file.name) && !notEmpty.test(file.name))
                {
                    if(fullinfo)
                    {
                        files.push(file);
                    }
                    else
                    {
                        files.push(file.name);
                    }
                } 
            });
            ok(files);
        }, function()
        {
            console.error('error list list sdcard ',received_files);
            reject();
        });
    });
};


Sdcard.prototype.getUsedSpace = function(sdcard_sub)
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        var request = sdcard_sub.usedSpace();
        request.onsuccess = function()
        {
            ok(this.result);
        };
        request.onerror= reject;
    });
};

Sdcard.prototype.getFreeSpace = function(sdcard_sub)
{
    return new Promise(function(ok, reject)
    {
        var request = sdcard_sub.freeSpace();
        request.onsuccess = function()
        {
            ok(this.result);
        };
        request.onerror= reject;
    });
};


Sdcard.prototype.search = function(search)
{
    var self=this;
    var files=[];
    var dirs=[];


    search = search.replace(/([^a-zA-Z0-9])/g,'\\$1');

     var re_file = new RegExp(search+'[^\/]*$','i');
     var re_dir = new RegExp('^(.*'+search+'[^\/]*\/).*','i');

     var notEmpty = new RegExp('\/\.empty');
    return new Promise(function(ok, reject)
    {
        var sdcard_selected=null;
        var p = [];
        self.sdcards.forEach(function(sdcard_sub)
        {
            p.push(self.list_sdcard(sdcard_sub).then(function(received_files)
            {
                received_files.forEach(function(file)
                {
                    if(re_file.test(file.name) && !notEmpty.test(file.name))
                    {
                        files.push(file);
                    } 
                    else if(re_dir.test(file.name))
                    {
                        var dir =  file.name.replace(re_dir,'$1');
                        if(dirs.indexOf(dir)===-1)
                        {
                            dirs.push(dir);
                        }
                    } 
                });
            }));
        });
        Promise.all(p).then(function()
        {
            ok({dirs: dirs, files: files});
        }, reject);
    });
};
