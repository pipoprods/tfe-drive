var CustomActivity = 
{
    activityReq: null,
    isPicking:false,
    isSharing: false,

    init: function()
    {
    },

    done_upload: function()
    {
        CustomActivity.isSharing = false;
    },
    send: function(blob)
    {
        CustomActivity.isPicking = false;
        CustomActivity.activityReq.postResult.type = blob.type;
        CustomActivity.activityReq.postResult({
            type: blob.type,
            name: blob.name,
            blob: blob
        });
    }
};

navigator.mozSetMessageHandler('activity', function (activityReq) {
    var option = activityReq.source;


    if(option.name =="share")
    {
        CustomActivity.activityReq = activityReq;
        CustomActivity.isSharing = true;
        // if app already started
        if(files.inited)
        {
            files.update();
        }
    }
    else if (option.name === "pick") {
        CustomActivity.activityReq = activityReq;
        CustomActivity.isPicking = true;
        // if app already started
        if(files.inited)
        {
            files.update();
        }
    }
});

