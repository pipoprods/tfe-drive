var Settings = function()
{
    this.apis=[];
};

Settings.prototype.bind = function()
{
    this.settings = document.querySelector('.settings');
    this.show_labels = document.querySelector('#show_labels');
    this.show_thumbnails = document.querySelector('#show_thumbnails');
    this.show_details = document.querySelector('#show_details');
    this.click_vibrate = document.querySelector('#click_vibrate');
    this.enable_cache = document.querySelector('#enable_cache');
    this.view_notification = document.querySelector('#view_notification');
    this.enable_sync = document.querySelector('#enable_sync');
    this.recent_time = document.querySelector('#recent_time');
    this.invalid_recent_time = document.querySelector('#invalid_recent_time');
    this.cache_location = document.querySelector('#cache_location');
    this.sync_time = document.querySelector('#sync_time');
    this.clear_cache = document.querySelector('#clear_cache');
    this.open_changelog = document.querySelector('#open_changelog');
};

Settings.prototype.init = function()
{
    var self=this;
    this.bind();

    // Add sdcards list
    sdcard.sdcards.forEach(function(sdcard_sub)
    {
        var option = document.createElement('option');
        option.value=sdcard_sub.storageName;
        option.innerHTML=sdcard_sub.storageName || translate('default_storage');
        self.cache_location.appendChild(option);
    });
    document.querySelector('#cache_location').addEventListener('change', function(e) { self.set_cachelocation(e.target.value, true); self.restoreSettings(); });
    document.querySelector('#sync_time').addEventListener('change', function(e) { self.set_synctime(e.target.value, true); self.restoreSettings(); });

    Array.forEach(document.querySelectorAll('.goto_settings'), function(item)
    {
        item.addEventListener('click', vibrate.button.bind(vibrate));
        item.addEventListener('click', self.open.bind(self));
    });

    // Toggle fieldset
    Array.forEach(document.querySelectorAll('fieldset.toggle legend'), function(item)
    {
        item.addEventListener('click', vibrate.button.bind(vibrate));
        item.addEventListener('click', self.toggle_legend.bind(self));
    });

    document.querySelector('#lang').addEventListener('change', function(e) { return self.set_lang(e.target.value, true); });

    this.show_labels.addEventListener('click', vibrate.button.bind(vibrate));
    this.show_labels.addEventListener('click', function(e) { return self.toggleShowLabels(e); });

    this.show_thumbnails.addEventListener('click', vibrate.button.bind(vibrate));
    this.show_thumbnails.addEventListener('click', function(e) { return self.toggleShowThumbnails(e); });

    this.show_details.addEventListener('click', vibrate.button.bind(vibrate));
    this.show_details.addEventListener('click', function(e) { return self.toggleShowDetails(e); });

    this.click_vibrate.addEventListener('click', vibrate.button.bind(vibrate));
    this.click_vibrate.addEventListener('click', function(e) { return self.toggleClickVibrate(e); });

    this.view_notification.addEventListener('click', vibrate.button.bind(vibrate));
    this.view_notification.addEventListener('click', function(e) { return self.toggleViewNotif(e); });

    this.enable_cache.addEventListener('click', vibrate.button.bind(vibrate));
    this.enable_cache.addEventListener('click', function(e) { return self.toggleEnableCache(e); });

    this.enable_sync.addEventListener('click', vibrate.button.bind(vibrate));
    this.enable_sync.addEventListener('click', function(e) { return self.toggleEnableSync(e); });

    this.clear_cache.addEventListener('click', vibrate.button.bind(vibrate));
    this.clear_cache.addEventListener('click', files.clearAllCache.bind(files));

    this.open_changelog.addEventListener('click', vibrate.button.bind(vibrate));
    this.open_changelog.addEventListener('click', this.openChangelog.bind(this));

    this.recent_time.addEventListener('change', function(e) { return self.updateRecentTime(e,1); });
    this.recent_time.addEventListener('keyup', function(e) { return self.updateRecentTime(e,0); });

    this.restoreSettings();
};

Settings.prototype.toggle_legend = function(item)
{
    var fieldset = item.target;
    while(fieldset && fieldset.tagName!=='FIELDSET')
    {
        fieldset = fieldset.parentNode;
    }
    if(fieldset.classList.contains('opened'))
    {
        fieldset.classList.remove('opened');
    }
    else
    {
        fieldset.classList.add('opened');
    }
};

Settings.prototype.open = function()
{
    this.settings.classList.add('focused');
};
Settings.prototype.close = function()
{
    this.settings.classList.remove('focused');
};

Settings.prototype.set_synctime=function(value)
{
    var options=[];
    var select = document.querySelector('#sync_time');
    Array.forEach(select.options, function(option)
    {
        options.push(option.value);
    });
    select.selectedIndex = options.indexOf(value+'');
    localStorage.setItem('syncTime', value);
};

Settings.prototype.set_cachelocation=function(value)
{
    var options=[];
    var select = document.querySelector('#cache_location');
    Array.forEach(select.options, function(option)
    {
        options.push(option.value);
    });
    select.selectedIndex = options.indexOf(value+'');
    localStorage.setItem('cacheLocation', value);
};

Settings.prototype.set_lang=function(lang, reload)
{
    var options=[];
    var select = document.querySelector('#lang');
    Array.forEach(select.options, function(option)
    {
        options.push(option.value);
    });
    select.selectedIndex = options.indexOf(lang+'');

    if(reload)
    {
        localStorage.setItem('lang', lang);
        location.reload();
    }
};
Settings.prototype.getLang= function()
{
    return localStorage.getItem('lang') || navigator.language;
};

Settings.prototype.restoreSettings= function()
{
    this.set_synctime(this.getSyncTime());
    this.set_cachelocation(this.getCacheLocation());
    this.set_lang(this.getLang());

    if(this.getEnablesync())
    {
        this.enable_sync.classList.remove('fa-toggle-off');
        this.enable_sync.classList.add('fa-toggle-on');
    }
    else
    {
        this.enable_sync.classList.add('fa-toggle-off');
        this.enable_sync.classList.remove('fa-toggle-on');
    }

    if(this.getViewNotif())
    {
        this.view_notification.classList.remove('fa-toggle-off');
        this.view_notification.classList.add('fa-toggle-on');
    }
    else
    {
        this.view_notification.classList.add('fa-toggle-off');
        this.view_notification.classList.remove('fa-toggle-on');
    }

    if(this.getEnableCache())
    {
        this.enable_cache.classList.remove('fa-toggle-off');
        this.enable_cache.classList.add('fa-toggle-on');
    }
    else
    {
        this.enable_cache.classList.add('fa-toggle-off');
        this.enable_cache.classList.remove('fa-toggle-on');
    }

    if(this.getClickVibrate())
    {
        this.click_vibrate.classList.remove('fa-toggle-off');
        this.click_vibrate.classList.add('fa-toggle-on');
    }
    else
    {
        this.click_vibrate.classList.add('fa-toggle-off');
        this.click_vibrate.classList.remove('fa-toggle-on');

    }

    if(this.getShowLabels())
    {
        this.show_labels.classList.remove('fa-toggle-off');
        this.show_labels.classList.add('fa-toggle-on');
        Array.forEach(document.querySelectorAll('.center_menu'), function(item)
        {
            item.classList.add('with_labels');
        });
    }
    else
    {
        this.show_labels.classList.add('fa-toggle-off');
        this.show_labels.classList.remove('fa-toggle-on');
        Array.forEach(document.querySelectorAll('.center_menu'), function(item)
        {
            item.classList.remove('with_labels');
        });

    }

    if(this.getShowThumbnails())
    {
        this.show_thumbnails.classList.remove('fa-toggle-off');
        this.show_thumbnails.classList.add('fa-toggle-on');
    }
    else
    {
        this.show_thumbnails.classList.add('fa-toggle-off');
        this.show_thumbnails.classList.remove('fa-toggle-on');

    }

    if(this.getShowDetails())
    {
        this.show_details.classList.remove('fa-toggle-off');
        this.show_details.classList.add('fa-toggle-on');
        files.files_container.classList.add('show_details');
    }
    else
    {
        this.show_details.classList.add('fa-toggle-off');
        this.show_details.classList.remove('fa-toggle-on');
        files.files_container.classList.remove('show_details');

    }
    this.recent_time.value= this.getRecentTime();

    sdcard.update_settings();
    sync.init();
};

Settings.prototype.toggleEnableSync= function(e)
{
    localStorage.setItem('enableSync', this.enable_sync.classList.contains('fa-toggle-off'));
    this.restoreSettings();
};

Settings.prototype.toggleViewNotif= function(e)
{
    localStorage.setItem('viewNotif', this.view_notification.classList.contains('fa-toggle-off'));
    this.restoreSettings();
};

Settings.prototype.toggleEnableCache= function(e)
{
    localStorage.setItem('enableCache', this.enable_cache.classList.contains('fa-toggle-off'));
    this.restoreSettings();
};

Settings.prototype.toggleShowLabels= function(e)
{
    localStorage.setItem('showLabels', this.show_labels.classList.contains('fa-toggle-off'));
    this.restoreSettings();
};
Settings.prototype.toggleShowThumbnails= function(e)
{
    localStorage.setItem('showThumbnails', this.show_thumbnails.classList.contains('fa-toggle-off'));
    this.restoreSettings();
};
Settings.prototype.toggleShowDetails= function(e)
{
    localStorage.setItem('showDetails', this.show_details.classList.contains('fa-toggle-off'));
    this.restoreSettings();
};

Settings.prototype.toggleClickVibrate= function(e)
{
    localStorage.setItem('clickVirate', this.click_vibrate.classList.contains('fa-toggle-off'));
    this.restoreSettings();
};

Settings.prototype.getSyncTime = function()
{
    var value= localStorage.getItem('syncTime');
    if(!value) { return 3600; } // default 3600 seconds =  1h
    return value;
};

Settings.prototype.getCacheLocation = function()
{
    var value= localStorage.getItem('cacheLocation');
    if(value===null) { return sdcard.sdcards[0].storageName; }
    return value; // default value, checked
};

Settings.prototype.getEnablesync = function()
{
    var value= localStorage.getItem('enableSync');
    if(value!==null) { return value==='true' ? true : false; }
    return true; // default value, checked
};

Settings.prototype.getViewNotif = function()
{
    var value= localStorage.getItem('viewNotif');
    if(value!==null) { return value==='true' ? true : false; }
    return true; // default value, checked
};

Settings.prototype.getEnableCache = function()
{
    var value= localStorage.getItem('enableCache');
    if(value!==null) { return value==='true' ? true : false; }
    return true; // default value, checked
};
Settings.prototype.getShowLabels = function()
{
    var value= localStorage.getItem('showLabels');
    if(value!==null) { return value==='true' ? true : false; }
    return true; // default value, checked
};
Settings.prototype.getShowThumbnails = function()
{
    var value= localStorage.getItem('showThumbnails');
    if(value!==null) { return value==='true' ? true : false; }
    return true; // default value, checked
};
Settings.prototype.getShowDetails = function()
{
    var value= localStorage.getItem('showDetails');
    if(value!==null) { return value==='true' ? true : false; }
    return false; // default value, unchecked
};
Settings.prototype.getClickVibrate = function()
{
    var value= localStorage.getItem('clickVirate');
    if(value!==null) { return value==='true' ? true : false; }
    return true; // default value, checked
};


Settings.prototype.updateRecentTime= function(e, do_warn)
{
    if(/^\d+$/.test(this.recent_time.value))
    {
        this.invalid_recent_time.classList.add('valid');
    }
    else
    {
        this.invalid_recent_time.classList.remove('valid');
        if(do_warn)
        {
           return files.alert(translate('invalid_recent_time'));
        }
    }

    if(do_warn)
    {
        localStorage.setItem('recentTime', this.recent_time.value);
        this.restoreSettings();
    }
};
Settings.prototype.openChangelog = function()
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        // Init XHR object
        var r = new XMLHttpRequest({ mozSystem: true });
        r.open('GET', 'Changelog.pm', true);
        r.setRequestHeader("ts",new Date());

        r.onreadystatechange = function () {
            if (r.readyState == 4)
            {
                if(r.status >= 200 && r.status< 400)
                {
                    // build selector
                    var changelog = r.responseText.split(/[\r\n]+/).splice(0,50);
                    var selector = new Selector();
                    selector.create(
                            '',
                            translate('changelog'),
                            changelog.join('<br>').replace(/#\s+([\d\.]+)/g,'<br><b>$1</b>'),
                            []);
                    ok();
                }
            }
        };
        r.send();
    });
};

Settings.prototype.getRecentTime = function()
{
    var value= localStorage.getItem('recentTime');
    if(value!==null) { return value; }
    return 7; // Default value
};
